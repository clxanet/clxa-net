# My blog and website

To compile, it needs [Rust](rust-lang.org), and a few extras installed:

```
$ rustup target add wasm32-unknown-unknown
$ cargo install -f wasm-bindgen-cli
```

To host locally,

```
$ cargo build-client
$ cargo prepare
$ cargo serve
```

To prepare a static archive for gitlab pages, 

```
# todo
```
