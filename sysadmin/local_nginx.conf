
worker_processes  10;
daemon off;

events {
    worker_connections  1024;
}

http {
    types {
        text/html                                        html;
        text/css                                         css;
        application/javascript                           js;
        image/jpeg                                       jpeg jpg;
        image/png                                        png;
        text/plain                                       txt;
        application/wasm                                 wasm;
        audio/ogg                                        ogg;
    }

    default_type       application/octet-stream;
    sendfile           on;
    keepalive_timeout  65;
    gzip               on;
    
    proxy_cache_path   logs/cache keys_zone=cache_em_all:10m;

    server {
        listen 8080;
        server_name localhost;

        error_page 500 502 503 504 /index.html;

        location / {
            root dist/;
            try_files $uri @index;
            add_header Cache-Control "public, immutable";
            expires 14d;
        }

        location @index {
            ssi on;
            root dist/;
            try_files /index.html =404;
        }

        location /.ssr {
            proxy_pass               http://127.0.0.1:8101/;
            proxy_set_header Host    localhost:8080;
            proxy_cache              cache_em_all;
            proxy_cache_valid        any 10m;
        }

        location /.multiplayer-timer {
            proxy_pass http://127.0.0.1:8100/.multiplayer-timer;
            proxy_http_version 1.1;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }
    }
}
