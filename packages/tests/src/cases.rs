use std::future::Future;

use fantoccini::{elements::Element, key::Key, wd::WindowHandle, Locator};
use spa::route::{Routable, Route};

use crate::prelude::*;

const NAME_LIST: &[&str] = &[
    "Bat", "Davis", "Deep", "Dennis", "Firen", "Freeze", "Henry", "Jan", "John", "Julian", "Louis",
    "Rudolf", "Woody",
];

const SHORT_ENOUGH_NAME_OR_SCORE: &str = "The string is just long enough";
const TOO_LONG_NAME_OR_SCORE: &str = "This string is too long for me!";

// test!("name", {
//     #[tracing::instrument(level = "debug", skip_all)]
//     async fn name(f: Fixture) -> Result<(), Report> {
//         let Fixture { mut progress, client, ..} = f;
//         tracing::info!("Short description for the test case");
//         progress.set_steps(0);
//         Err(eyre!("todo"))
//     }
//     name
// });

test!("home", {
    #[tracing::instrument(level = "debug", skip_all)]
    async fn home(f: Fixture) -> Result<(), Report> {
        let Fixture {
            mut progress,
            client,
            ..
        } = f;

        tracing::info!("GET / returns Route::Home");
        progress.set_steps(3);

        progress.step("navigate to /");
        client
            .goto("http://127.0.0.1:8080")
            .instrument(tracing::debug_span!("goto-page"))
            .await?;

        progress.step("get span[data-test=route]");
        let route_span = client
            .wait()
            .for_element(Locator::Css(r#"span[data-test=route]"#))
            .instrument(tracing::debug_span!("wait-for-element"))
            .await?;

        progress.step("get data-route");
        let route = route_span.attr("data-route").await?;

        progress.assert_eq(Some(format!("{:?}", Route::Home)), route)
    }
    home
});

test!("open-timer", {
    #[tracing::instrument(level = "debug", skip_all)]
    async fn open_timer(f: Fixture) -> Result<(), Report> {
        let Fixture {
            mut progress,
            client,
            ..
        } = f;

        tracing::info!("'Multiplayer Timer' navbar item leads to Route::Timer");
        progress.set_steps(4);

        progress.step("navigate to /");
        client
            .goto("http://127.0.0.1:8080")
            .instrument(tracing::debug_span!("goto-page"))
            .await?;

        progress.step("get 'Multiplayer Timer' link");
        let link = client
            .wait()
            .for_element(Locator::LinkText("Multiplayer Timer"))
            .instrument(tracing::debug_span!("wait-for-element"))
            .await?;

        progress.step("click 'Multiplayer Timer' link");
        link.click()
            .instrument(tracing::debug_span!("click-element"))
            .await?;

        progress.step("get current url");
        let url = client
            .current_url()
            .instrument(tracing::debug_span!("get-current-url"))
            .await?;

        progress.assert_eq(Route::Timer.to_path().as_str(), url.path())
    }
    open_timer
});

test!("timer-create-room", {
    #[tracing::instrument(level = "debug", skip_all)]
    async fn timer_create_room(f: Fixture) -> Result<(), Report> {
        let Fixture { .. } = f.clone();
        tracing::info!("Timer room setup tests");

        {
            let players = &[
                (NAME_LIST[0], "3"),
                (NAME_LIST[1], "7"),
                (NAME_LIST[2], "-34"),
                (NAME_LIST[3], "yes"),
                (NAME_LIST[4], "***"),
            ];

            with_timer_lobby(f.clone(), players, |lobby| async move {
                lobby.assert_player_list(players).await?;
                Ok(())
            })
            .await?;
        }

        with_timer_lobby(
            f.clone(),
            &[(SHORT_ENOUGH_NAME_OR_SCORE, SHORT_ENOUGH_NAME_OR_SCORE)],
            |lobby| async move {
                let name_validation = lobby
                    .get_text_input_validation(Locator::Css("span[data-test=name]"))
                    .await?;
                let score_validation = lobby
                    .get_text_input_validation(Locator::Css("span[data-test=score]"))
                    .await?;

                let name_report = (!name_validation.is_empty()).then(|| {
                    format!(
                        r#"name_validation='{name_validation}'
expected no validation message for the name since it is short enough"#
                    )
                });
                let score_report = (!score_validation.is_empty()).then(|| {
                    format!(
                        r#"score_validation='{score_validation}'
expected no validation message for the score since it is short enough"#
                    )
                });

                let reports = [name_report, score_report];
                if reports.iter().all(|r| r.is_none()) {
                    Ok(())
                } else {
                    Err(reports.into_iter().filter(Option::is_some).fold(
                        eyre!("validation errors despite length okay"),
                        |report, e| report.error(StrErr(e.unwrap())),
                    ))
                }
            },
        )
        .await?;

        with_timer_lobby(
            f,
            &[(TOO_LONG_NAME_OR_SCORE, TOO_LONG_NAME_OR_SCORE)],
            |lobby| async move {
                let name_validation = lobby
                    .get_text_input_validation(Locator::Css("input[data-test=name]"))
                    .await?;
                let score_validation = lobby
                    .get_text_input_validation(Locator::Css("input[data-test=score]"))
                    .await?;

                let name_report = (name_validation.is_empty()).then(|| {
                    "expected a validation message for the name since it is too long".to_string()
                });
                let score_report = (score_validation.is_empty()).then(|| {
                    "expected a validation message for the score since it is too long".to_string()
                });

                let reports = [name_report, score_report];
                if reports.iter().all(|r| r.is_none()) {
                    Ok(())
                } else {
                    Err(reports
                        .into_iter()
                        .filter(Option::is_some)
                        .fold(eyre!("length too long validation errors"), |report, e| {
                            report.error(StrErr(e.unwrap()))
                        }))
                }
            },
        )
        .await?;

        Ok(())
    }
    timer_create_room
});

test!("timer-chat", {
    #[tracing::instrument(level = "debug", skip_all)]
    async fn timer_chat(f: Fixture) -> Result<(), Report> {
        let Fixture { .. } = f.clone();

        let players = &[
            (NAME_LIST[0], "3"),
            (NAME_LIST[1], "7"),
            (NAME_LIST[2], "-34"),
            (NAME_LIST[3], "yes"),
            (NAME_LIST[4], "***"),
        ];

        tracing::info!("Timer chat tests");

        with_timer_lobby(f, players, move |lobby| {
            async move {
                // player 0 sends `Hello` and `World`
                lobby.switch_to_player(0).await?;
                lobby.submit_player_chat(&["Hello", "World"]).await?;

                //all players see `Hello World` from player 0
                let expected = [
                    format!("{}: {}", NAME_LIST[0], "Hello"),
                    format!("{}: {}", NAME_LIST[0], "World"),
                ];

                let log = lobby.get_session_log().await?;
                let deny = format!("{}: {}", NAME_LIST[0], "HelloWorld");
                if log.contains(&deny) {
                    return Err(eyre!("session log contains denied item")
                        .note(format!("item = {deny}"))
                        .note(format!(
                            "when player '{}' submits 'Hello' and then 'World`",
                            NAME_LIST[0]
                        )));
                }

                for i in 0..lobby.len() {
                    lobby.switch_to_player(i).await?;
                    let log = lobby.get_session_log().await?;
                    for ex in expected.iter() {
                        if !log.contains(ex) {}
                    }
                }
                Ok(())
            }
        })
        .await?;

        Ok(())
    }
    timer_chat
});

async fn with_timer_lobby<R, F: Future<Output = Result<R, Report>>>(
    fixture: Fixture,
    players: &[(&str, &str)],
    f: impl Fn(TimerLobby) -> F,
) -> Result<R, Report> {
    let lobby = TimerLobby::new(fixture, players).await?;
    let res = f(lobby.clone()).await;
    lobby.close().await?;
    res
}

#[derive(Clone)]
struct TimerLobby {
    fixture: Fixture,
    initial_window: WindowHandle,
    extra_windows: Vec<WindowHandle>,
}

impl TimerLobby {
    pub async fn new(mut fixture: Fixture, players: &[(&str, &str)]) -> Result<Self, Report> {
        // this function takes `players.len()` steps, and
        // initialize_lobby_players takes `players.len()` steps

        let url = create_timer_room(fixture.clone()).await?;
        fixture.progress.set_steps(players.len() * 2);
        fixture.progress.step("create timer room");

        let client = &fixture.client;
        let progress = &fixture.progress;

        let mut extra_windows = vec![];
        let initial_window = client.window().await?;
        let num_players = players.len();

        for i in 0..(num_players.saturating_sub(1)) {
            progress.step(&format!("create tab for player {}", i));
            match client.new_window(true).await {
                Ok(handle) => {
                    extra_windows.push(handle.handle.clone());
                }
                Err(e) => {
                    progress.fail();
                    Self {
                        fixture,
                        initial_window,
                        extra_windows,
                    }
                    .close()
                    .await?;
                    return Err(e.into());
                }
            };
        }

        let mut this = Self {
            fixture,
            initial_window,
            extra_windows,
        };

        if let Err(e) = this.initialize_lobby_players(&url, players).await {
            // if cannot open all windows, try to close all the ones it already opened
            this.close().await?;
            Err(e)
        } else {
            Ok(this)
        }
    }

    async fn initialize_lobby_players(
        &mut self,
        url: &str,
        players: &[(&str, &str)],
    ) -> Result<(), Report> {
        let client = &self.fixture.client;

        // start navigation
        for handle in self.extra_windows.iter().cloned() {
            client.switch_to_window(handle).await?;
            client.goto(url).await?;
        }

        // set player name and score
        for (i, (name, score)) in players.iter().enumerate() {
            self.fixture.progress.step(&format!(
                "set Player [{}] to {} with score {}",
                i, name, score
            ));
            self.switch_to_player(i).await?;
            self.set_player_name(name).await?;
            self.set_player_score(score).await?;
        }

        Ok(())
    }

    pub async fn close(self) -> Result<(), Report> {
        for handle in self.extra_windows.into_iter() {
            self.fixture.client.switch_to_window(handle).await?;
            self.fixture.client.close_window().await?;
        }
        self.fixture
            .client
            .switch_to_window(self.initial_window)
            .await?;
        Ok(())
    }

    /// return the total number of players/windows this lobby controls
    pub fn len(&self) -> usize {
        self.extra_windows.len() + 1
    }

    /// switch the current window in the client to the specified player's
    /// index. If the index is zero or out of range, switches to the initial window
    /// when the lobby was created
    pub async fn switch_to_player(&self, index: usize) -> Result<(), Report> {
        let window = if index == 0 {
            self.initial_window.clone()
        } else {
            self.extra_windows
                .get(index - 1)
                .unwrap_or(&self.initial_window)
                .clone()
        };
        self.fixture.client.switch_to_window(window).await?;
        Ok(())
    }

    pub async fn submit_text_input(
        &self,
        input: Locator<'_>,
        value: &str,
    ) -> Result<Element, Report> {
        let client = &self.fixture.client;

        let input = client
            .wait()
            .for_element(input)
            .instrument(tracing::debug_span!("wait-for-element"))
            .await?;

        // focus it
        input
            .click()
            .instrument(tracing::debug_span!("click"))
            .await?;

        // enter name
        input
            .send_keys(value)
            .instrument(tracing::debug_span!("send-keys"))
            .await?;

        // submit
        input
            .send_keys(&Key::Enter.to_string())
            .instrument(tracing::debug_span!("send-keys"))
            .await?;

        Ok(input)
    }

    pub async fn get_text_input_validation(&self, span: Locator<'_>) -> Result<String, Report> {
        let span = self
            .fixture
            .client
            .wait()
            .for_element(span)
            .instrument(tracing::debug_span!("wait-for-element"))
            .await?;

        Ok(span.text().await?.trim().to_string())
    }

    #[tracing::instrument(level = "debug", skip_all)]
    pub async fn set_player_name(&self, name: &str) -> Result<(), Report> {
        self.submit_text_input(Locator::Css("input[data-test=name]"), name)
            .await?;
        Ok(())
    }

    #[tracing::instrument(level = "debug", skip_all)]
    pub async fn set_player_score(&self, score: &str) -> Result<(), Report> {
        self.submit_text_input(Locator::Css("input[data-test=score]"), score)
            .await?;
        Ok(())
    }

    #[tracing::instrument(level = "debug", skip_all)]
    pub async fn submit_player_chat<S: AsRef<str>>(
        &self,
        messages: impl IntoIterator<Item = S>,
    ) -> Result<(), Report> {
        let mut messages = messages.into_iter();
        if let Some(message) = messages.next() {
            let input = self
                .submit_text_input(Locator::Css("input[data-test=chat]"), message.as_ref())
                .await?;

            for message in messages {
                // enter message
                input
                    .send_keys(message.as_ref())
                    .instrument(tracing::debug_span!("send-keys"))
                    .await?;

                // submit
                input
                    .send_keys(&Key::Enter.to_string())
                    .instrument(tracing::debug_span!("send-keys"))
                    .await?;
            }
        }
        Ok(())
    }

    pub async fn assert_player_list(&self, players: &[(&str, &str)]) -> Result<(), Report> {
        let mut expected_players: Vec<_> = players
            .iter()
            .map(|(name, score)| (name.to_string(), score.to_string()))
            .collect();
        expected_players.sort();

        for i in 0..self.len() {
            self.switch_to_player(i).await?;
            let players = self.get_player_list().await?;
            self.fixture
                .progress
                .assert_eq(&expected_players, &players)?;
        }
        Ok(())
    }

    #[tracing::instrument(level = "debug", skip_all)]
    pub async fn get_session_log(&self) -> Result<Vec<String>, Report> {
        let client = &self.fixture.client;

        let log_div = client
            .wait()
            .for_element(Locator::Css("div[data-test=session-log"))
            .instrument(tracing::debug_span!("wait-for-element"))
            .await?;

        let rows = log_div
            .find_all(Locator::Css("p"))
            .instrument(tracing::debug_span!("find-all"))
            .await?;

        let mut entries = vec![];
        for row in rows.into_iter() {
            let text = row.text().instrument(tracing::debug_span!("text")).await?;
            entries.push(text)
        }
        Ok(entries)
    }

    /// look for the player list in the current browser window and collect
    /// its names and scores into a sorted `Vec`
    #[tracing::instrument(level = "debug", skip_all)]
    pub async fn get_player_list(&self) -> Result<Vec<(String, String)>, Report> {
        let client = &self.fixture.client;

        let players_div = client
            .wait()
            .for_element(Locator::Css("tbody[data-test=players-list]"))
            .instrument(tracing::debug_span!("wait-for-element"))
            .await?;

        let rows = players_div
            .find_all(Locator::Css("tr"))
            .instrument(tracing::debug_span!("find-all"))
            .await?;

        let mut players_list = vec![];
        for row in rows.into_iter() {
            let cells = row
                .find_all(Locator::Css("td"))
                .instrument(tracing::debug_span!("find-all"))
                .await?;
            if cells.len() != 2 {
                return Err(eyre!("players list does not contain two cells per row"));
            }

            let name = cells[0]
                .text()
                .instrument(tracing::debug_span!("text"))
                .await?;

            let score = cells[1]
                .text()
                .instrument(tracing::debug_span!("text"))
                .await?;
            players_list.push((name, score));
        }

        players_list.sort();
        Ok(players_list)
    }
}

async fn create_timer_room(f: Fixture) -> Result<String, Report> {
    let Fixture {
        mut progress,
        client,
        ..
    } = f;

    progress.set_steps(7);

    // navigate to Route::Timer and make sure it's right

    progress.step("navigate to Route::Timer");
    client
        .goto(&("http://127.0.0.1:8080".to_string() + Route::Timer.to_path().as_str()))
        .instrument(tracing::debug_span!("goto-page"))
        .await?;

    // assert that route is correct

    progress.step("get span[data-test=route]");
    let route_span = client
        .wait()
        .for_element(Locator::Css("span[data-test=route]"))
        .instrument(tracing::debug_span!("wait-for-element"))
        .await?;

    progress.step("get data-route");
    let route = route_span.attr("data-route").await?;

    progress.assert_eq(Some(format!("{:?}", Route::Timer)), route)?;

    // create new room

    progress.step("get a[data-test=create-room]");
    let create_room = client
        .wait()
        .for_element(Locator::Css("a[data-test=create-room]"))
        .instrument(tracing::debug_span!("wait-for-element"))
        .await?;

    progress.step("click 'Create Room'");
    create_room
        .click()
        .instrument(tracing::debug_span!("wait-for-element"))
        .await?;

    // assert that in a room

    progress.step("get span[data-test=in-room]");
    client
        .wait()
        .for_element(Locator::Css("span[data-test=in-room]"))
        .instrument(tracing::debug_span!("wait-for-element"))
        .await?;

    Ok(client.current_url().await?.to_string())
}

#[derive(Debug)]
pub struct StrErr(String);

impl std::fmt::Display for StrErr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}
impl std::error::Error for StrErr {}
