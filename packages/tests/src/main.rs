//! End to end testing of clxa.net features
//!
//!

pub mod fixture;
pub mod timer;
pub mod types;

pub mod prelude {
    pub use crate::fixture::Fixture;
    pub use crate::test;
    pub use color_eyre::{
        eyre::{eyre, Context},
        Help, Report,
    };

    pub use tracing::Instrument;
}

use clap::{Args, Parser, Subcommand};
use prelude::*;
use types::TestCase;

#[tokio::main]
async fn main() -> Result<(), Report> {
    let cli = Runner::parse();

    color_eyre::install()?;
    cli.install_tracing()?;
    cli.execute().await?;
    Ok(())
}

/// End to end testing of clxa.net features
#[derive(Debug, Default, Parser)]
#[command(about, author, version)]
struct Runner {
    #[command(subcommand)]
    action: Option<RunnerSubcommands>,

    /// Enable verbose logging.
    #[arg(short)]
    pub v: bool,
}

impl Runner {
    pub fn install_tracing(&self) -> Result<(), Report> {
        use tracing_error::ErrorLayer;
        use tracing_subscriber::{fmt::format::FmtSpan, prelude::*};

        tracing_subscriber::registry()
            // Filter spans based on the RUST_LOG env var.
            .with(tracing_subscriber::EnvFilter::new(if self.v {
                "error,tests=debug"
            } else {
                "error,tests=info"
            }))
            // Send a copy of all spans to stdout.
            .with(
                tracing_subscriber::fmt::layer()
                    .with_target(false)
                    .with_level(true)
                    .with_span_events(FmtSpan::CLOSE)
                    .compact(),
            )
            // Add tracespans for color_eyre
            .with(ErrorLayer::default())
            // Install this registry as the global tracing registry.
            .try_init()?;
        tracing::info!("starting...");
        Ok(())
    }

    #[tracing::instrument(level = "info", skip_all)]
    pub async fn execute(self) -> Result<(), Report> {
        match self.action.unwrap_or_default() {
            RunnerSubcommands::List => {
                let cases = {
                    let _span = tracing::debug_span!("collect-tests");
                    let mut vec: Vec<_> = inventory::iter::<TestCase>().collect();
                    vec.sort_by(|a, b| a.name.cmp(b.name));
                    vec
                };

                for case in cases.into_iter() {
                    println!("{}", case.name);
                }
                Ok(())
            }
            RunnerSubcommands::Run(run) => run.execute().await,
        }
    }
}

#[derive(Debug, Subcommand)]
enum RunnerSubcommands {
    Run(Run),
    List,
}

impl Default for RunnerSubcommands {
    fn default() -> Self {
        Self::Run(Run::default())
    }
}

/// Run a test
#[derive(Debug, Default, Clone, Args)]
#[command(name = "run")]
pub struct Run {
    /// The test case to run [Default: all]
    #[arg()]
    pub test: Option<String>,

    /// Run the tests with a headless window [Default: off]
    #[arg(long, default_value_t = false)]
    pub headless: bool,
}

impl Run {
    pub async fn execute(self) -> Result<(), Report> {
        let fixture = Fixture::connect_default(self.headless).await?;
        let panic_guard = tokio::spawn({
            let fixture = fixture.clone();
            async move {
                fixture
                    .run_test(&self.test.unwrap_or("all".to_string()))
                    .await
            }
        });
        let res = match panic_guard.await {
            Ok(res) => res,
            Err(_) => Err(eyre!("test case has panicked")),
        };

        fixture.print_failures().await;
        fixture.close().await?;
        res
    }
}

#[macro_export]
macro_rules! test (
    ($name:expr, $run:expr) => (

            ::inventory::submit! {
                $crate::types::TestCase::new($name, &$run)
            }

    ));

#[cfg(test)]
mod tests {
    use crate::Runner;

    #[test]
    fn verify_cli() {
        use clap::CommandFactory;
        Runner::command().debug_assert();
    }
}
