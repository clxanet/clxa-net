use std::sync::Arc;

use crate::prelude::*;
use crate::types::TestCase;

use fantoccini::elements::Element;
use fantoccini::{Client, ClientBuilder, Locator};
use spa::route::{Routable, Route};
use tokio::sync::Mutex;

#[derive(Clone)]
#[non_exhaustive]
pub struct Fixture {
    pub client: Client,
    reports: Arc<Mutex<Vec<Report>>>,
}

impl Fixture {
    pub async fn connect_default(headless: bool) -> Result<Self, Report> {
        let mut client = ClientBuilder::native();

        if headless {
            client.capabilities(
                serde_json::from_str(r#"{"moz:firefoxOptions": {"args": ["--headless"]}}"#)
                    .wrap_err("bug: cannot create WebDriver capabilities object")?,
            );
        }

        let client = client
            .connect("http://127.0.0.1:4444")
            .await
            .wrap_err("Cannot connect to WebDriver")
            .suggestion("try running a WebDriver, such as geckodriver")?;

        Ok(Self {
            client,
            reports: Default::default(),
        })
    }

    pub async fn close(self) -> Result<(), Report> {
        tracing::info!("closing WebDriver session...");
        self.client
            .close()
            .await
            .wrap_err("Unable to close browser")
    }

    pub async fn navigate_to_route(&self, route: Route) -> Result<(), Report> {
        let url = "http://127.0.0.1:8080".to_string() + route.to_path().as_str();
        self.client.goto(&url).await?;
        Ok(())
    }

    pub async fn wait_for_element(&self, locator: Locator<'_>) -> Result<Element, Report> {
        self.client
            .wait()
            .for_element(locator)
            .await
            .wrap_err("cannot get element")
            .note(format!("locator={locator:?}"))
    }

    /// find test case by name
    ///
    ///
    /// # Error
    ///
    /// MissingCase: when no test case matches the case name
    pub async fn run_test(&self, case_name: &str) -> Result<(), Report> {
        let case = {
            let _span = tracing::debug_span!("find-case");
            let mut cases: Vec<_> = inventory::iter::<TestCase>().collect();
            cases.sort_by(|a, b| a.name.cmp(b.name));
            let index = cases
                .binary_search_by(|a| a.name.cmp(case_name))
                .map_err(|_| eyre!("cannot find test case '{case_name}'"))?;
            cases[index]
        };

        tracing::info!("RUN {case_name}");
        case.run(self.clone()).await
    }

    pub async fn print_failures(&self) {
        for report in self.reports.lock().await.iter() {
            println!("{report:?}")
        }
    }

    #[track_caller]
    pub fn failure(&self, report: impl AsRef<str>) {
        let report = eyre!(report.as_ref().to_string());
        let reports = self.reports.clone();
        tokio::spawn(async move { reports.lock().await.push(report) });
    }
}
