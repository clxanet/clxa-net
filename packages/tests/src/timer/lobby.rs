use fantoccini::{key::Key, wd::WindowHandle, Locator};
use spa::route::Route;

use crate::{
    prelude::*,
    timer::{INPUT_NAME, INPUT_SCORE},
};
use std::future::Future;

use super::{CREATE_ROOM_BUTTON, IN_ROOM_INDICATOR, PLAYERS_LIST, SESSION_LOG};

pub const PLAYERS: &[(&str, &str)] = &[
    ("Davis", "3"),
    ("Henry", "7"),
    ("Jan", "-34"),
    ("John", "yes"),
    ("Louis", "***"),
];

pub async fn with_timer_lobby<R, F: Future<Output = Result<R, Report>>>(
    fixture: Fixture,
    f: impl Fn(Fixture, TimerLobby) -> F,
) -> Result<R, Report> {
    let lobby = TimerLobby::new(fixture.clone(), PLAYERS).await?;
    let res = f(fixture.clone(), lobby.clone()).await;
    lobby.close().await?;
    res
}

async fn create_timer_room(f: Fixture) -> Result<String, Report> {
    f.navigate_to_route(Route::Timer).await?;

    let create_room_button = f.wait_for_element(CREATE_ROOM_BUTTON).await?;
    create_room_button.click().await?;
    f.wait_for_element(IN_ROOM_INDICATOR).await?;

    Ok(f.client.current_url().await?.to_string())
}

#[derive(Clone)]
pub struct TimerLobby {
    fixture: Fixture,
    initial_window: WindowHandle,
    extra_windows: Vec<WindowHandle>,
}

impl TimerLobby {
    pub async fn new(f: Fixture, players: &[(&str, &str)]) -> Result<Self, Report> {
        let url = create_timer_room(f.clone()).await?;
        tracing::info!(?url, "lobby room");

        let mut extra_windows = vec![];
        let initial_window = f.client.window().await?;
        let num_players = players.len();

        for _ in 0..(num_players.saturating_sub(1)) {
            match f.client.new_window(true).await {
                Ok(handle) => {
                    extra_windows.push(handle.handle.clone());
                }
                Err(e) => {
                    Self {
                        fixture: f,
                        initial_window,
                        extra_windows,
                    }
                    .close()
                    .await?;
                    return Err(e.into());
                }
            };
        }

        let mut this = Self {
            fixture: f,
            initial_window,
            extra_windows,
        };

        if let Err(e) = this.initialize_lobby_players(&url, players).await {
            // if cannot open all windows, try to close all the ones it already opened
            this.close().await?;
            Err(e)
        } else {
            Ok(this)
        }
    }

    async fn initialize_lobby_players(
        &mut self,
        url: &str,
        players: &[(&str, &str)],
    ) -> Result<(), Report> {
        // start navigation
        for handle in self.extra_windows.iter().cloned() {
            self.fixture.client.switch_to_window(handle).await?;
            self.fixture.client.goto(url).await?;
        }

        // set player name and score
        for (i, (name, score)) in players.iter().enumerate() {
            tracing::info!(player = i, %name, %score, "set Player");
            self.switch_to_player(i).await?;
            self.submit_inputs(INPUT_NAME, &[name]).await?;
            self.submit_inputs(INPUT_SCORE, &[score]).await?;
        }

        Ok(())
    }

    pub async fn close(self) -> Result<(), Report> {
        for handle in self.extra_windows.into_iter() {
            self.fixture.client.switch_to_window(handle).await?;
            self.fixture.client.close_window().await?;
        }
        self.fixture
            .client
            .switch_to_window(self.initial_window)
            .await?;
        Ok(())
    }

    /// return the total number of players/windows this lobby controls
    #[allow(clippy::len_without_is_empty)]
    pub fn len(&self) -> usize {
        self.extra_windows.len() + 1
    }

    /// switch the current window in the client to the specified player's
    /// index.
    ///
    /// If the index is zero or out of range, switch to the initial window
    /// when the lobby was created
    pub async fn switch_to_player(&self, index: usize) -> Result<(), Report> {
        let window = if index == 0 {
            self.initial_window.clone()
        } else {
            self.extra_windows
                .get(index - 1)
                .unwrap_or(&self.initial_window)
                .clone()
        };
        self.fixture.client.switch_to_window(window).await?;
        Ok(())
    }

    pub async fn submit_inputs<S: AsRef<str>>(
        &self,
        locator: Locator<'_>,
        lines: impl IntoIterator<Item = S>,
    ) -> Result<(), Report> {
        let mut lines = lines.into_iter();
        if let Some(line) = lines.next() {
            let client = &self.fixture.client;

            let input = client.wait().for_element(locator).await?;

            // focus it
            input.click().await?;

            // enter name
            input.send_keys(line.as_ref()).await?;

            // submit
            input.send_keys(&Key::Enter.to_string()).await?;

            for line in lines {
                // enter message
                input.send_keys(line.as_ref()).await?;

                // submit
                input.send_keys(&Key::Enter.to_string()).await?;
            }
        }
        Ok(())
    }

    pub async fn get_current_player_name(&self) -> Result<String, Report> {
        self.fixture
            .wait_for_element(INPUT_NAME)
            .await?
            .prop("value")
            .await?
            .ok_or_else(|| eyre!("{INPUT_NAME:?} found but it has not 'value' attribute"))
    }

    /// look for the player list in the current browser window and collect
    /// its names and scores into a sorted `Vec`
    pub async fn get_player_list(&self) -> Result<Vec<(String, String)>, Report> {
        let players_div = self.fixture.wait_for_element(PLAYERS_LIST).await?;

        let rows = players_div.find_all(Locator::Css("tr")).await?;

        let mut players_list = vec![];
        for row in rows.into_iter() {
            let cells = row.find_all(Locator::Css("td")).await?;
            if cells.len() != 2 {
                return Err(eyre!("players list does not contain two cells per row"));
            }

            let name = cells[0].text().await?;

            let score = cells[1].text().await?;
            players_list.push((name, score));
        }

        players_list.sort();
        Ok(players_list)
    }

    /// tests each player's list of players compared to `PLAYERS`
    /// Return an Err when the WebDriver fails,
    /// or return Ok(Some(String)) with a message of the first failure
    pub async fn assert_player_list(&self) -> Result<Option<String>, Report> {
        let mut expected_players: Vec<_> = PLAYERS
            .iter()
            .map(|(name, score)| (name.to_string(), score.to_string()))
            .collect();
        expected_players.sort();

        for i in 0..self.len() {
            self.switch_to_player(i).await?;
            let players = self.get_player_list().await?;
            if players != expected_players {
                return Ok(Some(format!(
                    "player {i} does not have the expected player list"
                )));
            }
        }
        Ok(None)
    }

    pub async fn one_log_prefix_count(&self, player: &str, content: &str) -> Result<u32, Report> {
        let log_div = self.fixture.wait_for_element(SESSION_LOG).await?;
        let rows = log_div.find_all(Locator::Css("p")).await?;

        let mut count = 0;
        for row in rows.into_iter() {
            let text = row.text().await?;
            if text.starts_with(player) && text[player.len()..].trim().starts_with(content.trim()) {
                count += 1;
            }
        }
        Ok(count)
    }

    pub async fn all_logs_prefix_min_count(
        &self,
        player: &str,
        content: &str,
    ) -> Result<u32, Report> {
        let mut count = u32::MAX;
        for i in 0..self.len() {
            self.switch_to_player(i).await?;
            count = count.min(self.one_log_prefix_count(player, content).await?)
        }
        Ok(count)
    }

    pub async fn one_log_equal_count(&self, player: &str, content: &str) -> Result<u32, Report> {
        let log_div = self.fixture.wait_for_element(SESSION_LOG).await?;
        let rows = log_div.find_all(Locator::Css("p")).await?;

        let mut count = 0;
        for row in rows.into_iter() {
            let text = row.text().await?;
            if text.starts_with(player) && content.trim() == text[player.len()..].trim() {
                count += 1;
            }
        }
        Ok(count)
    }

    pub async fn all_logs_equal_min_count(
        &self,
        player: &str,
        content: &str,
    ) -> Result<u32, Report> {
        let mut count = u32::MAX;
        for i in 0..self.len() {
            self.switch_to_player(i).await?;
            count = count.min(self.one_log_equal_count(player, content).await?)
        }
        Ok(count)
    }
}
