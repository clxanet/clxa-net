//! Integration tests for timer pages

pub mod lobby;

use std::time::Duration;

use crate::prelude::*;
use fantoccini::Locator;
use lobby::with_timer_lobby;

pub const CREATE_ROOM_BUTTON: Locator<'_> = Locator::Css("a[data-test=create-room]");
pub const IN_ROOM_INDICATOR: Locator<'_> = Locator::Css("span[data-test=in-room]");
pub const INPUT_NAME: Locator<'_> = Locator::Css("input[data-test=name");
pub const VALIDATION_NAME: Locator<'_> = Locator::Css("span[data-test=name");
pub const INPUT_SCORE: Locator<'_> = Locator::Css("input[data-test=score");
pub const VALIDATION_SCORE: Locator<'_> = Locator::Css("span[data-test=score");
pub const INPUT_CHAT: Locator<'_> = Locator::Css("input[data-test=chat");
pub const VALIDATION_CHAT: Locator<'_> = Locator::Css("span[data-test=chat");
pub const INPUT_TIMER: Locator<'_> = Locator::Css("input[data-test=timer");
pub const VALIDATION_TIMER: Locator<'_> = Locator::Css("div[data-test=timer");
pub const PLAYERS_LIST: Locator<'_> = Locator::Css("tbody[data-test=players-list]");
pub const SESSION_LOG: Locator<'_> = Locator::Css("div[data-test=session-log");
pub const TOGGLE_TIMER: Locator<'_> = Locator::Css("button[data-test=toggle-timer");

const SHORT_ENOUGH: &str = "The string is just long enough";
const TOO_LONG: &str = "This string is too long for me!";
test!("all", |f: Fixture| async move {
    f.run_test("timer").await?;
    Ok(())
});

test!("timer", |f: Fixture| async move {
    f.run_test("timer::room").await?;
    f.run_test("timer::local_player").await?;
    f.run_test("timer::chat").await?;
    f.run_test("timer::clock").await?;
    Ok(())
});

test!("timer::local_player", |f: Fixture| async move {
    f.run_test("timer::local_player::short").await?;
    f.run_test("timer::local_player::long").await?;
    Ok(())
});

test!("timer::chat", |f: Fixture| async move {
    f.run_test("timer::chat::submit_synchronized").await?;
    f.run_test("timer::chat::submit_also_clears_input").await?;
    f.run_test("timer::chat::deny_empty_messages").await?;
    Ok(())
});

test!("timer::clock", |f: Fixture| async move {
    f.run_test("timer::clock::stop_start_has_single_stop_message")
        .await?;
    f.run_test("timer::clock::time_out_automatically_stops")
        .await?;
    Ok(())
});

test!("timer::room", |f: Fixture| with_timer_lobby(
    f,
    |f, lobby| {
        async move {
            if let Some(err) = lobby.assert_player_list().await? {
                f.failure(err);
            }
            Ok(())
        }
    }
));

test!("timer::local_player::short", |f: Fixture| with_timer_lobby(
    f,
    |f, lobby| async move {
        lobby.submit_inputs(INPUT_NAME, &[SHORT_ENOUGH]).await?;
        lobby.submit_inputs(INPUT_SCORE, &[SHORT_ENOUGH]).await?;

        let validation = f.wait_for_element(VALIDATION_NAME).await?.text().await?;
        if !validation.trim().is_empty() {
            f.failure(format!(
                "expected no validation error when name is short enough. got {validation:?}"
            ));
        }

        let validation = f.wait_for_element(VALIDATION_SCORE).await?.text().await?;
        if !validation.trim().is_empty() {
            f.failure(format!(
                "expected no validation error when score is short enough. got {validation:?}"
            ));
        }
        Ok(())
    }
));

test!("timer::local_player::long", |f: Fixture| with_timer_lobby(
    f,
    move |f, lobby| {
        async move {
            lobby.submit_inputs(INPUT_NAME, &[TOO_LONG]).await?;
            lobby.submit_inputs(INPUT_SCORE, &[TOO_LONG]).await?;

            let validation = f.wait_for_element(VALIDATION_NAME).await?.text().await?;
            if validation.trim().is_empty() {
                f.failure("expected a validation message when name is too long. got none");
            }

            let validation = f.wait_for_element(VALIDATION_SCORE).await?.text().await?;
            if validation.trim().is_empty() {
                f.failure("expected a validation message when score is too long. got none");
            }

            Ok(())
        }
    }
));

test!("timer::chat::submit_synchronized", |f: Fixture| {
    with_timer_lobby(f, |f, lobby| async move {
        lobby.submit_inputs(INPUT_CHAT, &["Hello World"]).await?;
        let name = lobby.get_current_player_name().await?;
        if lobby
            .all_logs_equal_min_count(&name, ": Hello World")
            .await?
            == 0
        {
            f.failure("'Hello World' was not received by all players");
        }
        Ok(())
    })
});

test!("timer::chat::submit_also_clears_input", |f: Fixture| {
    with_timer_lobby(f, |f, lobby| async move {
        lobby.submit_inputs(INPUT_CHAT, &["Hello", "World"]).await?;
        let name = lobby.get_current_player_name().await?;
        if lobby
            .all_logs_equal_min_count(&name, ": HelloWorld")
            .await?
            > 0
        {
            f.failure(
                    "forbidden message 'HelloWorld' was received when sending two distinct 'Hello' and 'World' messages"
                );
        }

        if lobby.all_logs_equal_min_count(&name, ": Hello").await? == 0 {
            f.failure("'Hello' was not received by all players");
        }

        if lobby.all_logs_equal_min_count(&name, ": World").await? == 0 {
            f.failure("'World' was not received by all players");
        }

        Ok(())
    })
});

test!("timer::chat::deny_empty_messages", |f: Fixture| {
    with_timer_lobby(f, |f, lobby| async move {
        lobby.submit_inputs(INPUT_CHAT, &[""]).await?;
        let name = lobby.get_current_player_name().await?;
        if lobby.one_log_equal_count(&name, ":").await? > 0 {
            f.failure("forbidden message '' was received when sending a '' message");
        }
        Ok(())
    })
});

test!(
    "timer::clock::stop_start_has_single_stop_message",
    |f: Fixture| {
        with_timer_lobby(f, |f, lobby| async move {
            let name = lobby.get_current_player_name().await?;

            // start and stop a 1 second countdown
            lobby.submit_inputs(INPUT_TIMER, &["1"]).await?;
            f.wait_for_element(TOGGLE_TIMER).await?.click().await?;
            f.wait_for_element(TOGGLE_TIMER).await?.click().await?;

            // start and stop it again
            f.wait_for_element(TOGGLE_TIMER).await?.click().await?;
            f.wait_for_element(TOGGLE_TIMER).await?.click().await?;

            let num_stops = lobby
                .all_logs_prefix_min_count(&name, "has stopped timer")
                .await?;
            if num_stops != 3 {
                f.failure(format!("expected 3 stop timer messages, got {num_stops}"));
            }

            Ok(())
        })
    }
);

test!(
    "timer::clock::time_out_automatically_stops",
    |f: Fixture| {
        with_timer_lobby(f, |f, lobby| async move {
            // start a 1 second countdown
            lobby.submit_inputs(INPUT_TIMER, &["1"]).await?;
            f.wait_for_element(TOGGLE_TIMER).await?.click().await?;

            tokio::time::sleep(Duration::from_secs(2)).await;
            let text = f.wait_for_element(TOGGLE_TIMER).await?.text().await?;
            if text != "Start" {
                f.failure(format!(
                    "toggle timer button text is '{text}'. expected 'Start'"
                ));
            }
            Ok(())
        })
    }
);
