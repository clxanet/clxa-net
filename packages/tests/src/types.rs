use crate::prelude::*;

use async_trait::async_trait;
use std::future::Future;

inventory::collect!(TestCase);

pub struct TestCase {
    pub name: &'static str,
    pub runner: &'static dyn TestRun,
}

impl TestCase {
    pub const fn new(name: &'static str, runner: &'static dyn TestRun) -> Self {
        Self { name, runner }
    }

    pub async fn run(&self, f: Fixture) -> Result<(), Report> {
        self.runner.run(f).await
    }
}

trait TestFn {
    type Output;
    fn call(&self, f: Fixture) -> Self::Output;
}

impl<F, T> TestFn for F
where
    F: Fn(Fixture) -> T,
{
    type Output = T;
    fn call(&self, f: Fixture) -> Self::Output {
        self(f)
    }
}

#[async_trait]
pub trait TestRun: Send + Sync + 'static {
    async fn run(&self, f: Fixture) -> Result<(), Report>;
}

#[async_trait]
impl<F> TestRun for F
where
    F: TestFn + Send + Sync + 'static,
    F::Output: Future<Output = Result<(), Report>> + Send,
{
    async fn run(&self, f: Fixture) -> Result<(), Report> {
        self.call(f).await
    }
}
