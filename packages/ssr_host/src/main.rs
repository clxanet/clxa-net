use axum::{
    extract::{Host, Path, Query},
    http::Method,
    response::IntoResponse,
    routing::get,
    Router, Server,
};
use spa::prelude::*;
use std::collections::HashMap;
use tracing_subscriber::prelude::*;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| "info".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let app = Router::new().route("/*rest", get(render_path));
    let address = "127.0.0.1:8101".parse().unwrap();

    tracing::info!("listening for ssr API calls on {}", address);
    Server::bind(&address)
        .serve(app.into_make_service())
        .await
        .unwrap();
}

async fn render_path(
    method: Method,
    Host(host): Host,
    Path(url): Path<String>,
    Query(args): Query<HashMap<String, String>>,
) -> impl IntoResponse {
    tracing::info!(?host, ?method, ?url, ?args);
    let renderer = yew::ServerRenderer::<spa::App>::with_props(move || spa::Props {
        host: Some(host),
        url: Some(url),
        args: Some(args),
    });
    renderer.render().await
}
