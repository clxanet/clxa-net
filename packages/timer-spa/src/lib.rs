use spa::pages::timer::hooks::TimeType;
pub use yew;

use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use yew::prelude::*;
mod room_config;
mod server_config;

pub const API_ROUTE: &str = "/.manage";

#[derive(Serialize, Deserialize)]
pub enum SetRoomConfig {
    MaxPlayerCount { room_id: u32, count: usize },
    MaxRoomCount { count: usize },
    ForceCreateRoom,
}

#[derive(Debug, Default, Clone, PartialEq, Properties, Serialize, Deserialize)]
pub struct Props {
    #[prop_or_default]
    pub host: Option<String>,

    #[prop_or_default]
    pub url: Option<String>,

    #[prop_or_default]
    pub args: Option<HashMap<String, String>>,

    #[prop_or_default]
    pub rooms: Option<ServerSummary>,
}

#[derive(Debug, Default, Clone, PartialEq, Serialize, Deserialize)]
pub struct ServerSummary {
    pub max_room_count: usize,
    pub rooms: Vec<RoomSummary>,
}

#[derive(Debug, Default, Clone, PartialEq, Serialize, Deserialize)]
pub struct RoomSummary {
    pub present: bool,
    pub room_id: u32,
    pub current_player_count: usize,
    pub max_player_count: usize,
    pub expires_in: TimeType,
}

#[function_component]
pub fn App(props: &Props) -> Html {
    let props = use_prepared_state!(|_| -> Props { props.clone() }, ());
    let prop_state = use_state_eq(Props::default);
    if let Ok(Some(ref props)) = props {
        prop_state.set(Props::clone(props));
    }

    html! {
        <InnerApp ..{(*prop_state).clone()} />
    }
}

#[function_component]
fn InnerApp(props: &Props) -> Html {
    let rooms = props.rooms.clone().unwrap_or_default();
    let i1 = rooms.rooms.iter().step_by(3).cloned();
    let i2 = rooms
        .rooms
        .iter()
        .skip(1)
        .step_by(3)
        .cloned()
        .chain(std::iter::repeat(RoomSummary::default()));
    let i3 = rooms
        .rooms
        .iter()
        .skip(2)
        .step_by(3)
        .cloned()
        .chain(std::iter::repeat(RoomSummary::default()));

    let rooms_iter = i1.zip(i2.zip(i3));
    html! {
        <>
        if let Some(rooms) = &props.rooms {
            <server_config::Component current_room_count={rooms.rooms.len()} max_room_count={rooms.max_room_count} />
            <div class="section">
                <div class="tile is-ancestor is-vertical">{
                    rooms_iter
                        .map(|(r1, (r2, r3))| {
                            html! {
                                <div class="tile">
                                    <room_config::Component room={r1}/>
                                    <room_config::Component room={r2}/>
                                    <room_config::Component room={r3}/>
                                </div>
                            }
                        })
                        .collect::<Html>()
                }</div>
            </div>
        }
        </>
    }
}
