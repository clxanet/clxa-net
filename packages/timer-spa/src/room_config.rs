use crate::RoomSummary;
use gloo::net::http::Request;
use spa::{components::submit_line, pages::timer::clock::time_to_str_val_without_millis};
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub room: Option<RoomSummary>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    if let Some(room) = &props.room {
        let onsubmit = Callback::from({
            let room_id = room.room_id;
            move |val: String| {
                if let Ok(count) = val.parse() {
                    yew::platform::spawn_local(async move {
                        if Request::post(crate::API_ROUTE)
                            .json(&crate::SetRoomConfig::MaxPlayerCount { room_id, count })
                            .unwrap()
                            .send()
                            .await
                            .is_ok()
                        {
                            gloo::utils::window().location().reload().ok();
                        }
                    })
                }
            }
        });

        html! {
            if room.present {
                <div class="tile is-parent is-4">
                    <div class="tile is-child notification">
                        <div class="section">
                            <div class="">
                                {"room_id: "}
                                {&room.room_id}
                            </div>
                            <div class="">
                                {"expires in "}{time_to_str_val_without_millis(room.expires_in)}
                            </div>
                            <div class="">
                                {&room.current_player_count}
                                {"/"}
                                {room.max_player_count}
                            </div>
                            <div class="">
                                <submit_line::Component cta="set max" {onsubmit} />
                            </div>
                        </div>
                    </div>
                </div>
            }
        }
    } else {
        html! {}
    }
}
