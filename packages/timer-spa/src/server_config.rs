use gloo::net::http::Request;
use spa::components::submit_line;
use yew::prelude::*;

#[derive(PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub current_room_count: usize,

    #[prop_or_default]
    pub max_room_count: usize,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        current_room_count,
        max_room_count,
    } = props;

    let onclick = Callback::from(|ev: MouseEvent| {
        ev.prevent_default();
        yew::platform::spawn_local(async move {
            if Request::post(crate::API_ROUTE)
                .json(&crate::SetRoomConfig::ForceCreateRoom)
                .unwrap()
                .send()
                .await
                .is_ok()
            {
                gloo::utils::window().location().reload().ok();
            }
        })
    });

    let onsubmit = Callback::from({
        move |val: String| {
            if let Ok(count) = val.parse() {
                yew::platform::spawn_local(async move {
                    if Request::post(crate::API_ROUTE)
                        .json(&crate::SetRoomConfig::MaxRoomCount { count })
                        .unwrap()
                        .send()
                        .await
                        .is_ok()
                    {
                        gloo::utils::window().location().reload().ok();
                    }
                })
            }
        }
    });

    html! {
        <div class="hero is-primary">
            <div class="hero-body">
                <h1 class="title">{"clxa.net - timer management panel"}</h1>
                <div class="container">
                    <div class="columns">
                        <div class="column"><button class="button is-warning" {onclick}>{"create room"}</button></div>
                        <h1 class="title column">{"Rooms: "}{current_room_count}{"/"}{max_room_count}</h1>
                        <div class="column"><submit_line::Component cta="set max" {onsubmit} /></div>
                    </div>
                </div>
            </div>
        </div>
    }
}
