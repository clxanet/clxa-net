use spa::{App, Props};

#[cfg(debug_assertions)]
fn main() {
    yew::Renderer::<App>::with_props(Props::default()).render();
}

#[cfg(not(debug_assertions))]
fn main() {
    yew::Renderer::<App>::with_props(Props::default()).hydrate();
}
