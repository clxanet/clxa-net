use crate::documents;
use crate::pages::*;
use crate::prelude::*;
pub use yew_router::Routable;

pub const NAV_ITEMS: &[(&str, Route)] = &[("Multiplayer Timer", Route::Timer)];

#[derive(Debug, Clone, Copy, PartialEq, Routable)]
pub enum Route {
    #[not_found]
    #[at("/404")]
    NotFound,

    #[at("/")]
    Home,

    #[at("/multiplayer-timer")]
    Timer,

    #[at("/tos/202307")]
    TosLatest,
}

pub fn switch(route: Route) -> Html {
    html! {
        <>
        <SetTitle {route} />
        <span data-test="route" data-route={format!("{route:?}")} />
        {
            match route {
                Route::Home => html! { <home::Page /> },
                Route::Timer => html! { <timer::Page /> },
                Route::TosLatest => html! {<documents::Tos202307 />},
                Route::NotFound => html! { <main><h1>{ "404 - NOT FOUND" }</h1></main> },
             }
        }</>
    }
}

pub const fn title_of(route: Route) -> &'static str {
    match route {
        Route::NotFound => "Not Found - clxa.net",
        Route::Home => "clxa.net",
        Route::Timer => "Multiplayer Timer - clxa.net",
        Route::TosLatest => "Terms and Conditions - clxa.net",
    }
}

#[derive(Clone, Copy, PartialEq, Properties)]
struct Props {
    route: Route,
}

#[function_component]
fn SetTitle(props: &Props) -> Html {
    let Props { route } = *props;
    use_effect(move || {
        if let Some(window) = web_sys::window() {
            if let Some(document) = window.document() {
                document.set_title(title_of(route))
            }
        }
    });
    html! {}
}
