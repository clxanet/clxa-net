use crate::prelude::*;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub host: HostName,

    pub children: Children,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props { host, children } = props.clone();
    html! {
        <ContextProvider<HostName> context={host}>
            {children}
        </ContextProvider<HostName>>
    }
}

#[derive(Clone, PartialEq)]
pub struct HostName(AttrValue);

impl HostName {
    pub fn new(host: String) -> Self {
        Self(host.into())
    }

    pub fn as_str(&self) -> &str {
        &self.0
    }
}
