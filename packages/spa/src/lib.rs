pub mod components;
pub mod context;
pub mod documents;
pub mod pages;
pub mod route;

pub mod prelude {
    pub use crate::{components::*, route::Route};
    pub use wasm_bindgen::prelude::*;
    pub use yew::{self, prelude::*};
    pub use yew_router::prelude::*;
}

use std::collections::HashMap;

use prelude::*;
use yew_router::history::{AnyHistory, History, MemoryHistory};
#[derive(Default, Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub host: Option<String>,

    #[prop_or_default]
    pub url: Option<String>,

    #[prop_or_default]
    pub args: Option<HashMap<String, String>>,
}

#[function_component]
pub fn App(props: &Props) -> Html {
    let Props {
        host: _host,
        url,
        args,
    } = props.clone();
    let ssr_host = use_transitive_state!(move |_| -> Option<String> { _host }, ());
    let host = match ssr_host {
        Ok(None) => props.host.clone().unwrap(),
        Err(_) => get_hostname_fe().unwrap(),
        Ok(Some(hostname)) => hostname.as_ref().clone().unwrap(),
    };
    let host = context::HostName::new(host);

    let page = html! {
        <context::Component {host}>
            <navbar::Component />
            <Switch<Route> render={route::switch} />
            <footer::Component />
        </context::Component>

    };

    if let (Some(url), Some(args)) = (url, args) {
        let history = AnyHistory::from(MemoryHistory::new());
        history.push_with_query(&*url, args).unwrap();
        html! {
            <Router {history}>
                {page}
            </Router>
        }
    } else {
        html! {
            <BrowserRouter>
                {page}
            </BrowserRouter>
        }
    }
}

fn get_hostname_fe() -> Option<String> {
    web_sys::window()?.location().host().ok()
}
