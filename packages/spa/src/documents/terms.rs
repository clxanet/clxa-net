use crate::prelude::*;

#[function_component]
pub fn Tos202307() -> Html {
    html! {
        <main>
            <div class="hero is-primary">
                <div class="hero-body">
                    <h1 class="title">{"Terms and Conditions"}</h1>
                    <p class="subtitle">{"Last updated: July 2023"}</p>
                </div>
            </div>

            <div class="section content">

            <h2>{"Personal Data"}</h2>
            <ul>
                <li>{r#"
The website only stores data you explicitly provided, and only for the duration of your visit.
"#}</li>
                <li>{r#"
Multiplayer sessions of the website will share the data with other users in the session.
"#}</li>
                <li>
                    {"Your interactions with the website are handled by "}
                    <a href="https://www.ovhcloud.com/en-ie/" rel="noreferrer noopener" target="_blank">
                        {"OVHcloud."}
                    </a>
                </li>
            </ul>

            <h2>{"Terms"}</h2>
            <p>{r#"
    Copyright © 20xx <copyright holders>"#}</p>

            <p>{r#"
Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:"#}</p>

            <p>{r#"
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
"#}</p>

            <p>{r#"
THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"#}</p>

            <h2>{"Do Not"}</h2>
            <ul>
                <li>{r#"
Use the website in any manner that could interfere with,
disable, disrupt, overburden, or otherwise impair the website;"#}
                </li>
                <li>{r#"
Gain access to (or attempt to gain access to) another user’s data or any
non-public portions of the website, including the computer systems or networks
connected to or used together with the website;"#}</li>
            <li>{r#"
Upload, transmit, or distribute to or through the website any malicious code or
other data intended to interfere with any computer systems;"#}</li>
            </ul>
            </div>
        </main>
    }
}
