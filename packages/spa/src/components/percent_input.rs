use crate::prelude::*;
use web_sys::HtmlInputElement;

#[derive(Debug, PartialEq, Properties)]
pub struct Props {
    pub ident: AttrValue,
    pub label: AttrValue,

    #[prop_or_default]
    pub initial_value: AttrValue,

    pub oninput: Callback<u8>,

    #[prop_or_default]
    pub classes: Classes,

    #[prop_or_default]
    pub data_test: Option<AttrValue>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        ident,
        label,
        initial_value,
        oninput,
        classes,
        data_test,
    } = props;

    let volume = use_state(|| initial_value.to_string());

    let oninput = {
        let oninput = oninput.clone();
        Callback::from({
            let volume = volume.clone();
            move |ev: InputEvent| {
                if let Some(target) = ev.target() {
                    if let Ok(node) = target.dyn_into::<HtmlInputElement>() {
                        if let Ok(value) = node.value().parse() {
                            oninput.emit(value);
                            volume.set(value.to_string());
                        }
                    }
                }
            }
        })
    };

    html! {
        <div>
            <label for={ident}>{{label}}</label>
            <input
            data-test={data_test}
                class={classes.clone()}
                type="range"
                id={ident}
                name={ident}
                min="0" max="100" value={volume.to_string()} step="10" {oninput} />
            <datalist id="markers">
                <option value="0"></option>
                <option value="25"></option>
                <option value="50"></option>
                <option value="75"></option>
                <option value="100"></option>
            </datalist>
        </div>
    }
}
