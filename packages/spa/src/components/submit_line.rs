use crate::prelude::*;

#[derive(Debug, PartialEq, Properties)]
pub struct Props {
    #[prop_or("Send".into())]
    pub cta: AttrValue,

    #[prop_or_default]
    pub placeholder: AttrValue,

    #[prop_or(false)]
    pub disabled: bool,

    #[prop_or_default]
    pub validation_message: AttrValue,

    #[prop_or_default]
    pub validation_classes: Classes,

    #[prop_or_default]
    pub initial_value: AttrValue,

    #[prop_or_default]
    pub oninput: Callback<String>,

    #[prop_or_default]
    pub onsubmit: Callback<String>,

    #[prop_or_default]
    pub data_test: Option<AttrValue>,

    #[prop_or_default]
    pub clear_on_submit: bool,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        cta,
        placeholder,
        disabled,
        validation_message,
        validation_classes,
        initial_value,
        oninput,
        onsubmit,
        data_test,
        clear_on_submit,
    } = props;
    let disabled = *disabled;

    let validation_message = if validation_message.is_empty() {
        "\u{a0}" // &nbsp;
    } else {
        validation_message
    };

    let state = use_state(move || initial_value.to_string());
    let value = state.to_string();

    let oninput = {
        let oninput = oninput.clone();
        let state = state.clone();
        Callback::from(move |new_value: String| {
            oninput.emit(new_value.clone());
            state.set(new_value);
        })
    };

    let onclick = {
        let clear_on_submit = *clear_on_submit;
        let onsubmit = onsubmit.clone();
        Callback::from(move |ev: MouseEvent| {
            ev.prevent_default();
            if clear_on_submit {
                state.set(String::new());
            }
            onsubmit.emit(state.to_string())
        })
    };

    let validation_classes = classes!("help", validation_classes.clone());

    html! {
        <form>
            <div class="field has-addons">
                <div class="control is-expanded">
                    <text_input::Component {data_test} {placeholder} classes="input" {value} {oninput} />
                    <span data-test={data_test} class={validation_classes}>{validation_message}</span>
                </div>
                <div class="control">
                    <button class="button is-link" {onclick} {disabled}>{cta}</button>
                </div>
            </div>
        </form>
    }
}
