use crate::prelude::*;
use web_sys::HtmlInputElement;

#[derive(Debug, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub value: Option<AttrValue>,
    pub oninput: Callback<String>,

    #[prop_or_default]
    pub placeholder: Option<AttrValue>,

    #[prop_or_default]
    pub classes: Classes,

    #[prop_or_default]
    pub data_test: Option<AttrValue>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        ref value,
        ref oninput,
        ref placeholder,
        ref classes,
        ref data_test,
    } = props;

    let oninput = {
        let oninput = oninput.clone();
        Callback::from(move |ev: InputEvent| {
            if let Some(target) = ev.target() {
                if let Ok(node) = target.dyn_into::<HtmlInputElement>() {
                    oninput.emit(node.value());
                }
            }
        })
    };

    let focusing_click = use_state_eq(|| true);

    let onclick = Callback::from({
        let focusing_click = focusing_click.clone();
        move |ev: MouseEvent| {
            if *focusing_click {
                focusing_click.set(false);
                if let Some(target) = ev.target() {
                    if let Ok(node) = target.dyn_into::<HtmlInputElement>() {
                        node.select();
                        ev.prevent_default();
                    }
                }
            }
        }
    });

    let onblur = Callback::from({
        let focusing_click = focusing_click;
        move |_ev: FocusEvent| {
            focusing_click.set(true);
        }
    });

    html! {
        <input data-test={data_test} {placeholder} class={classes.clone()} {value} {oninput} {onclick} {onblur} />
    }
}
