use crate::prelude::*;

#[function_component]
pub fn Component() -> Html {
    let burger_visible = use_state(|| false);
    let onclick = Callback::from({
        let burger_visible = burger_visible.clone();
        move |ev: MouseEvent| {
            ev.prevent_default();
            burger_visible.set(!*burger_visible);
        }
    });
    let navbar_burger_classes = classes!("navbar-burger", (*burger_visible).then_some("is-active"));
    let navbar_menu_classes = classes!("navbar-menu", (*burger_visible).then_some("is-active"));

    let location = use_location().unwrap().path().to_string();
    let nav_items = crate::route::NAV_ITEMS
        .iter()
        .map(|(name, route)| {
            if location == route.to_path() {
                html! {
                    <Link<Route> classes={"navbar-item has-text-dark"} to={*route}>
                    <strong>{name}</strong>
                    </Link<Route>>
                }
            } else {
                html! {
                    <Link<Route> classes={"navbar-item"} to={*route}>
                    {name}
                    </Link<Route>>
                }
            }
        })
        .collect::<Html>();

    html! {
        <nav>
            <div class="navbar" role="navigation" aria-label="main navigation">
                <div class="navbar-brand">
                    <Link<Route> to={Route::Home} classes="navbar-item">
                        <img
                            src="/clxa-36ef53100356aaed.png"
                            alt="Logo: a white on green super-position of the letters CLXA and a tiny, white wind rose on its right." />
                        {"\u{a0}clxa.net"}
                    </Link<Route>>

                    // a burger menu for mobile
                    <a role="button" class={navbar_burger_classes} aria-label="menu" aria-expanded="false" {onclick} >
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                        <span aria-hidden="true"></span>
                    </a>
                </div>

                <div class={navbar_menu_classes}>
                    {nav_items}
                </div>
            </div>
            <hr class="has-background-primary"/>
        </nav>
    }
}
