use crate::prelude::*;

#[function_component]
pub fn Component() -> Html {
    html! {
        <footer class="footer">
            <div>
                <ul>
                    <li><Link<Route> to={Route::TosLatest}>{"terms of service"}</Link<Route>></li>
                </ul>
            </div>
            <div class="level-item has-text-centered">
                <ul>
                    <a href="mailto:clxanet@pm.me">{"clxanet@pm.me"}</a>{"\u{a0}© all rights reserved."}
                </ul>
            </div>
        </footer>
    }
}
