use crate::prelude::*;

#[function_component]
pub fn Page() -> Html {
    html! {
        <main>
            <div class="hero is-primary">
                <div class="hero-body">
                    <h1 class="title">{"clxa.net"}</h1>
                    <p class="subtitle">{"Creations of a code alchemist."}</p>
                </div>
            </div>
            <div class="section">
                <ul>
                <li><Link<Route> to={Route::Timer}>{"Multiplayer Timer"}</Link<Route>>{" - A Timer to share with your friends"}</li>
                </ul>
                <div class="section"></div>
            </div>
        </main>
    }
}
