use crate::prelude::*;

#[derive(Debug, Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub children: ChildrenWithProps<PlayerInfo>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props { ref children } = props;
    html! {
        <tbody data-test="players-list">
            {for children.iter()}
        </tbody>
    }
}

#[derive(Debug, Clone, PartialEq, Properties)]
pub struct PlayerProps {
    pub name: AttrValue,
    pub score: AttrValue,
}

#[function_component]
pub fn PlayerInfo(props: &PlayerProps) -> Html {
    html! {
        <tr>
            <td>{&props.name}</td>
            <td>{&props.score}</td>
        </tr>
    }
}
