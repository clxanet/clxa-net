use super::{
    clock,
    hooks::{use_connection, ClientPlayer},
    local_player, players, session_log,
    sound::{self, Sound},
};
use crate::prelude::*;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub room_id: AttrValue,
}

#[function_component]
pub fn Page(props: &Props) -> Html {
    let Props { room_id } = props.clone();

    let host = use_context::<crate::context::HostName>().unwrap();
    let host = host.as_str();
    let room = use_connection(host, &room_id);
    let (name, set_name) = room.name_state();
    let (score, set_score) = room.score_state();
    let (time, set_time) = room.time_state();
    let (log_items, onsubmit) = room.log_state();
    let all_players = room.all_players();
    let error_message = room.error_message();
    let expiry = room.room_expiry();

    let next_sound = use_state(|| sound::Sound::Silence);
    let clear_next_sound = Callback::from({
        let next_sound = next_sound.clone();
        move |()| next_sound.set(Sound::Silence)
    });
    let set_next_sound = Callback::from({
        let next_sound = next_sound.clone();
        move |value: sound::Sound| next_sound.set(value)
    });

    html! {
        if error_message.is_empty() {
            <div class="section">
                <span data-test="in-room" />
                <p class="has-text-centered">{"hint: give your friend the link of this page"}</p>
                <div class="level is-align-items-flex-start">
                    <div class="level-item">
                        <div class="box">
                            <clock::Component queued_sound={*next_sound} {set_next_sound} {time} {set_time} {expiry} />
                            <sound::Component next_sound={*next_sound}   {clear_next_sound}/>
                        </div>
                    </div>
                    <div class="level-item">
                        <div class="box">
                            <local_player::Component {name} {score} {set_name} {set_score} />
                            <hr />
                            <div class="table-container">
                                <table class="table is-fullwidth">
                                    <thead>
                                        <tr>
                                            <th>{"Name"}</th>
                                            <th>{"Score"}</th>
                                        </tr>
                                    </thead>
                                    <players::Component>
                                        {for all_players.into_iter()
                                            .map(|ClientPlayer { name, score }| html_nested!{<players::PlayerInfo {name} {score}/>})}
                                    </players::Component>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="section">
                <div class="box" data-test="session-log">
                    <submit_line::Component data_test="chat" placeholder="chat" {onsubmit} clear_on_submit={true} />
                    <session_log::Component>
                        {for log_items.borrow().iter().cloned().map(session_log::Item::from)}
                    </session_log::Component>
                </div>
            </div>
        } else {
            <div class="section">
                <h1 class="subtitle">{error_message}</h1>
            </div>
        }
    }
}
