pub mod clock;
pub mod create_room;
pub mod hooks;
pub mod local_player;
pub mod play_room;
pub mod players;
pub mod session_log;
pub mod sound;

use crate::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Debug, Default, Serialize, Deserialize)]
pub struct Query {
    pub room_id: Option<String>,
}

#[function_component]
pub fn Page() -> Html {
    let Query { room_id } = use_location().unwrap().query().unwrap_or_default();

    html! {
        <main>
            <div class="hero is-primary">
                <div class="hero-body">
                    <h1 class="title">{"Multiplayer Timer"}</h1>
                    <p class="subtitle">{"A Timer to share with your friends"}</p>
                </div>
            </div>
            if let Some(room_id) = room_id {
                <play_room::Page {room_id} />
            }
             else {
                <create_room::Page />
             }
        </main>
    }
}
