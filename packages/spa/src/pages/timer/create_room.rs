use std::time::Duration;

use crate::prelude::*;
use gloo::net::http::Request;
use serde::{Deserialize, Serialize};
use yew::platform::{self, time};

const API_ROUTE: &str = "/.multiplayer-timer/create-room/";

const TIMEOUT: Duration = Duration::from_secs(15);

#[derive(Serialize, Deserialize)]
pub enum CreateRoomResponse {
    Created { room_id: String },
}

impl CreateRoomResponse {
    pub fn to_json(self) -> String {
        serde_json::to_string(&self).unwrap()
    }

    pub fn from_json(value: &str) -> Option<Self> {
        serde_json::from_str(value).ok()
    }
}

#[function_component]
pub fn Page() -> Html {
    let navigator = use_navigator().unwrap();
    let first_load = use_state(|| true);
    let request_in_progress = use_state(|| true);
    let error_message = use_state(|| "Loading Page...".to_string());

    use_effect({
        let request_in_progress = request_in_progress.clone();
        let error_message = error_message.clone();
        move || {
            if *first_load {
                first_load.set(false);
                request_in_progress.set(false);
                error_message.set(String::new());
            }
        }
    });

    let onclick = Callback::from({
        let request_in_progress = request_in_progress.clone();
        let error_message = error_message.clone();
        move |ev: MouseEvent| {
            ev.prevent_default();
            if *request_in_progress {
                return;
            }

            request_in_progress.set(true);
            error_message.set(String::new());
            platform::spawn_local({
                let request_in_progress = request_in_progress.clone();
                async move {
                    time::sleep(TIMEOUT).await;
                    request_in_progress.set(false);
                }
            });
            platform::spawn_local({
                let navigator = navigator.clone();
                let request_in_progress = request_in_progress.clone();
                let error_message = error_message.clone();
                async move {
                    match Request::post(API_ROUTE).send().await {
                        Err(e) => error_message.set(format!("cannot send request: {e}")),
                        Ok(res) => {
                            if res.status() != 200 {
                                error_message
                                    .set("bad response from server. try again later".to_string());
                            } else {
                                match res.json::<CreateRoomResponse>().await {
                                    Ok(CreateRoomResponse::Created { room_id }) => {
                                        let room_id = Some(room_id);
                                        navigator
                                            .push_with_query(
                                                &Route::Timer,
                                                &super::Query { room_id },
                                            )
                                            .unwrap();
                                    }
                                    Err(e) => error_message.set(format!("cannot get request: {e}")),
                                }
                            }
                        }
                    }

                    // wait 3 more seconds to stop spammy users
                    time::sleep(Duration::from_secs(3)).await;
                    request_in_progress.set(false);
                }
            });
        }
    });

    html! {
        <div class="section">
            if *request_in_progress {
                <p class="button has-background-primary has-text-white">
                <span class="loader" />{"\u{a0}Create a Room"}
                </p>
            } else {
                <a data-test="create-room" class="button is-link" {onclick}>
                    {"Create a Room"}
                </a>
            }
            <p class="has-text-danger">{&*error_message}</p>
            <div class="section"></div>
        </div>
    }
}
