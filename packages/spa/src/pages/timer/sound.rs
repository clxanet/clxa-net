use web_sys::HtmlAudioElement;

use crate::prelude::*;

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
    pub next_sound: Sound,
    pub clear_next_sound: Callback<()>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        next_sound,
        clear_next_sound,
    } = props.clone();

    let sounds = use_sounds_nodes();
    let volume_enabled = use_state(|| false);
    let set_volume = Callback::from({
        let sounds = sounds.clone();
        let volume_enabled = volume_enabled.clone();
        move |value: u8| {
            volume_enabled.set(value != 0);
            for sound in sounds.iter() {
                if let Some(node) = sound.get() {
                    if let Ok(audio) = node.dyn_into::<HtmlAudioElement>() {
                        audio.set_volume(f64::from(value) / 100.0);
                    }
                }
            }
        }
    });
    let current = use_state(|| Sound::Silence);
    let onended = Callback::from({
        let current = current.clone();
        move |_ev: Event| {
            current.set(Sound::Silence);
        }
    });

    let html = html! {
        <div class="section has-text-centered">
            <percent_input::Component ident="volume" label="Volume: " initial_value="0" oninput={set_volume}/>
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::One).unwrap()}     src="/timer/1.ogg" data-test="one" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Two).unwrap()}     src="/timer/2.ogg" data-test="two" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Three).unwrap()}   src="/timer/3.ogg" data-test="three" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Four).unwrap()}    src="/timer/4.ogg" data-test="four" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Five).unwrap()}    src="/timer/5.ogg" data-test="five" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Ten).unwrap()}     src="/timer/10.ogg" data-test="ten" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Thirty).unwrap()}  src="/timer/30.ogg" data-test="thirty" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Beat).unwrap()}    src="/timer/beat.ogg" data-test="beat" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Pause).unwrap()}   src="/timer/pause.ogg" data-test="pause" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Done).unwrap()}    src="/timer/done.ogg" data-test="done" />
            <audio onended={onended.clone()} ref={get_node(&sounds, Sound::Minute).unwrap()}  src="/timer/minute.ogg" data-test="minute" />
            <audio onended={onended}         ref={get_node(&sounds, Sound::Minutes).unwrap()} src="/timer/minutes.ogg" data-test="minutes" />
        </div>
    };

    use_effect({
        move || {
            // if `current`, wait for it to end
            if let Some(node) = get_node(&sounds, *current) {
                if !node.cast::<HtmlAudioElement>().unwrap_throw().ended() {
                    return;
                }
            }
            // if audio enabled by visitor
            if *volume_enabled {
                if let Some(next_node) = get_node(&sounds, next_sound) {
                    let audio = next_node.cast::<HtmlAudioElement>().unwrap_throw();
                    audio.set_current_time(0.0);
                    audio.play().ok();
                    current.set(next_sound);
                    clear_next_sound.emit(());
                } else {
                    let audio = sounds[usize::from(Sound::Beat) - 1]
                        .clone()
                        .cast::<HtmlAudioElement>()
                        .unwrap_throw();
                    audio.set_current_time(0.0);
                    audio.play().ok();
                }
            } else if next_sound != Sound::Silence {
                // a sound was queued, but the visitor does not want to hear it,
                // so silently clear it.
                clear_next_sound.emit(());
            }
        }
    });

    html
}

fn get_node(sounds: &[NodeRef], sound: Sound) -> Option<NodeRef> {
    if sound == Sound::Silence {
        None
    } else {
        Some(sounds[usize::from(sound) - 1].clone())
    }
}

#[hook]
fn use_sounds_nodes() -> Vec<NodeRef> {
    let one = use_node_ref();
    let two = use_node_ref();
    let three = use_node_ref();
    let four = use_node_ref();
    let five = use_node_ref();
    let ten = use_node_ref();
    let thirty = use_node_ref();
    let beat = use_node_ref();
    let rock = use_node_ref();
    let pause = use_node_ref();
    let done = use_node_ref();
    let minute = use_node_ref();
    let minutes = use_node_ref();
    vec![
        one, two, three, four, five, ten, thirty, beat, rock, pause, done, minute, minutes,
    ]
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Sound {
    Silence,
    One,
    Two,
    Three,
    Four,
    Five,
    Ten,
    Thirty,
    Beat,
    Pause,
    Done,
    Minute,
    Minutes,
}

impl From<usize> for Sound {
    fn from(value: usize) -> Self {
        match value {
            1 => Self::One,
            2 => Self::Two,
            3 => Self::Three,
            4 => Self::Four,
            5 => Self::Five,
            6 => Self::Ten,
            7 => Self::Thirty,
            8 => Self::Beat,
            9 => Self::Pause,
            10 => Self::Done,
            11 => Self::Minute,
            12 => Self::Minutes,
            _ => Self::Silence,
        }
    }
}

impl From<Sound> for usize {
    fn from(value: Sound) -> Self {
        match value {
            Sound::Silence => 0,
            Sound::One => 1,
            Sound::Two => 2,
            Sound::Three => 3,
            Sound::Four => 4,
            Sound::Five => 5,
            Sound::Ten => 6,
            Sound::Thirty => 7,
            Sound::Beat => 8,
            Sound::Pause => 9,
            Sound::Done => 10,
            Sound::Minute => 11,
            Sound::Minutes => 12,
        }
    }
}
