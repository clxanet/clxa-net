use yew::{html::ChildrenRenderer, virtual_dom::VChild};

use crate::prelude::*;

use super::hooks;

#[derive(Debug, Clone, PartialEq, Properties)]
pub struct Props {
    #[prop_or_default]
    pub children: ChildrenRenderer<Item>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props { ref children } = props;

    children.iter().collect()
}

#[derive(Debug, Clone, PartialEq)]
pub enum Item {
    PlayerChange(VChild<PlayerChange>),
    Message(VChild<Message>),
    Countdown(VChild<Countdown>),
}

impl From<hooks::SessionMessage> for Item {
    fn from(value: hooks::SessionMessage) -> Self {
        match value {
            hooks::SessionMessage::PlayerChange { name, content } => {
                Item::PlayerChange(html_nested!(<PlayerChange {name} {content} />))
            }
            hooks::SessionMessage::Message { name, content } => {
                Item::Message(html_nested!(<Message {name} {content} />))
            }
            hooks::SessionMessage::Countdown { name, content } => {
                Item::Countdown(html_nested!(<Countdown {name} {content} />))
            }
        }
    }
}

impl From<Item> for Html {
    fn from(value: Item) -> Self {
        match value {
            Item::PlayerChange(child) => child.into(),
            Item::Message(child) => child.into(),
            Item::Countdown(child) => child.into(),
        }
    }
}

impl From<VChild<PlayerChange>> for Item {
    fn from(value: VChild<PlayerChange>) -> Self {
        Self::PlayerChange(value)
    }
}

impl From<VChild<Message>> for Item {
    fn from(value: VChild<Message>) -> Self {
        Self::Message(value)
    }
}

impl From<VChild<Countdown>> for Item {
    fn from(value: VChild<Countdown>) -> Self {
        Self::Countdown(value)
    }
}

#[derive(Debug, Clone, PartialEq, Properties)]
pub struct ItemProps {
    pub name: Option<String>,

    #[prop_or_default]
    pub content: String,
}

#[function_component]
pub fn PlayerChange(props: &ItemProps) -> Html {
    let ItemProps {
        ref name,
        ref content,
    } = props;

    html! {
        <p>
            if let Some(player) = name {
                {player}{" has "}
            }
            {content}
        </p>
    }
}

#[function_component]
pub fn Message(props: &ItemProps) -> Html {
    let ItemProps {
        ref name,
        ref content,
    } = props;

    html! {
        <p>
            if let Some(player) = name {
                {player}{": "}
            }
            {content}
        </p>
    }
}

#[function_component]
pub fn Countdown(props: &ItemProps) -> Html {
    let ItemProps {
        ref name,
        ref content,
    } = props;

    html! {
        <p>
            if let Some(player) = name {
                {player}{" has "}
            }
            {content}
        </p>
    }
}
