use gloo::timers::callback::Interval;

use crate::prelude::*;
use std::num::ParseIntError;

use super::{
    hooks::{TimeState, TimeType},
    sound::Sound,
};

const FAST_CLOCK_UPDATE: u32 = 1;
const SLOW_CLOCK_UPDATE: u32 = 166;

#[derive(PartialEq, Properties)]
pub struct Props {
    pub time: TimeState,
    pub set_time: Callback<TimeState>,
    pub expiry: (f64, TimeType),
    pub queued_sound: Sound,
    pub set_next_sound: Callback<Sound>,
}

#[derive(Clone, Copy)]
struct RunningTime {
    current_time: f64,
    state: TimeState,
    time_of_expiry: f64,
    expiry: TimeType,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        time,
        set_time,
        expiry,
        queued_sound,
        set_next_sound,
    } = props;

    let running_time = use_state(|| Option::<RunningTime>::None);
    let interval = use_mut_ref(|| Option::<Interval>::None);

    use_effect_with_deps(
        {
            let running_time = running_time.clone();
            let set_next_sound = set_next_sound.clone();
            move |(time, expiry): &(TimeState, (f64, TimeType))| {
                let (time_of_expiry, expiry) = *expiry;
                let current_time = js_sys::Date::now();
                let state = *time;
                let value = Some(RunningTime {
                    current_time,
                    state,
                    time_of_expiry,
                    expiry,
                });
                let previous_is_running = running_time.map(|t| t.state.is_running).unwrap_or(false);
                let is_running = state.is_running;
                running_time.set(value);

                if previous_is_running != is_running && previous_is_running {
                    set_next_sound.emit(Sound::Pause);
                }

                *interval.borrow_mut() = Some(Interval::new(
                    if is_running {
                        FAST_CLOCK_UPDATE
                    } else {
                        SLOW_CLOCK_UPDATE
                    },
                    {
                        let running_time = running_time.clone();
                        move || {
                            // trigger a redraw
                            running_time.set(value);
                        }
                    },
                ));

                move || {
                    if let Some(interval) = interval.borrow_mut().take() {
                        interval.cancel();
                    }
                }
            }
        },
        (*time, *expiry),
    );

    let sound_minute = use_mut_ref(|| false);
    let sound_minutes = use_mut_ref(|| false);
    let sound_maybe = use_mut_ref(|| Option::<Sound>::None);
    let previous_clock_val = use_mut_ref(|| 0);

    let now = js_sys::Date::now();
    let clock;
    let expires;
    let expires_classes;
    if let Some(RunningTime {
        current_time: then,
        state: TimeState {
            is_running, time, ..
        },
        time_of_expiry,
        expiry,
    }) = *running_time
    {
        let clock_val = if is_running {
            let elapsed = (now - then) as TimeType;
            time.saturating_sub(elapsed)
        } else {
            *previous_clock_val.borrow_mut() = time;
            time
        };

        if let Some(sound) = sound_cue_minutes(clock_val, *previous_clock_val.borrow()) {
            *sound_minutes.borrow_mut() = true;
            *sound_maybe.borrow_mut() = Some(sound);
        }

        if let Some(sound) = sound_cue_minute(clock_val, *previous_clock_val.borrow()) {
            *sound_minute.borrow_mut() = true;
            *sound_maybe.borrow_mut() = Some(sound);
        }

        if let Some(sound) = sound_cue_seconds(clock_val, *previous_clock_val.borrow()) {
            *sound_maybe.borrow_mut() = Some(sound);
        }

        if is_running && clock_val == 0 {
            running_time.set(None);
            set_time.emit(TimeState {
                is_running: false,
                time: 0,
                no_send: true,
            });
            set_next_sound.emit(Sound::Done);
        }

        *previous_clock_val.borrow_mut() = clock_val;

        let expires_val = expiry.saturating_sub((now - time_of_expiry) as TimeType);

        clock = time_to_str_val(clock_val);
        expires = time_to_str_val_without_millis(expires_val);
        if expires_val < 15 * 60 * 1000 {
            expires_classes = "is-warning";
        } else {
            expires_classes = "is-light";
        }
    } else {
        clock = "xx:xx:xx.xxx".to_string();
        expires = "xx:xx:xx".to_string();
        expires_classes = "is-light";
    };
    let expires_classes = classes!("tag", "is-medium", expires_classes);

    if *queued_sound == Sound::Silence {
        let sound_maybe_value = *sound_maybe.borrow();
        if let Some(sound) = sound_maybe_value {
            *sound_maybe.borrow_mut() = None;
            set_next_sound.emit(sound);
        } else if *sound_minutes.borrow() {
            *sound_minutes.borrow_mut() = false;
            set_next_sound.emit(Sound::Minutes);
        } else if *sound_minute.borrow() {
            *sound_minute.borrow_mut() = false;
            set_next_sound.emit(Sound::Minute);
        }
    }

    let onsubmit = Callback::from({
        let time = *time;
        let set_time = set_time.clone();
        move |s: String| {
            if let Ok(millis) = time_str_to_val(&s) {
                set_time.emit(TimeState {
                    time: 1000 * millis,
                    no_send: false,
                    ..time
                });
            } else {
                gloo::console::log!("not setting time with bad string");
            }
        }
    });

    let error_state = use_state(|| None);
    let validation_message = error_state.as_ref().map(String::from).unwrap_or_default();

    let oninput = Callback::from(move |s: String| {
        error_state.set(time_str_to_val(&s).map_err(|e| e.to_string()).err())
    });

    let cta = if time.is_running { "Stop" } else { "Start" };
    let start_stop_class = if time.is_running {
        "is-danger"
    } else {
        "is-primary"
    };

    let onclick = {
        Callback::from({
            let time = *time;
            let set_time = set_time.clone();

            move |ev: MouseEvent| {
                ev.prevent_default();

                let current_time = if let Some(RunningTime {
                    current_time: then,
                    state:
                        TimeState {
                            is_running, time, ..
                        },
                    ..
                }) = *running_time
                {
                    if is_running {
                        let now = js_sys::Date::now();
                        let elapsed = (now - then) as TimeType;
                        time.saturating_sub(elapsed)
                    } else {
                        time
                    }
                } else {
                    time.time
                };

                if current_time > 0 {
                    set_time.emit(TimeState {
                        is_running: !time.is_running,
                        time: current_time,
                        ..Default::default()
                    });
                }
            }
        })
    };

    html! {
        <div class="section has-text-centered">
            <h1 class={classes!("title", "is-1", "is-family-code", "has-text-danger", "has-text-centered")}>{clock}</h1>
            <hr />
            <div class="level">
                <div class="level-item control">
                    <button data-test="toggle-timer" class={classes!("button", start_stop_class)} {onclick}>{cta}</button>
                </div>
            </div>
            <submit_line::Component
                data_test="timer"
                cta="Set"
                validation_classes="is-danger"
                {validation_message}
                {onsubmit}
                {oninput}
                disabled={time.is_running}
                placeholder="10:00"
            />
            <div class={expires_classes}>{"This room expires in "}{expires}</div>
        </div>
    }
}

#[derive(Debug)]
pub enum MalformedTime {
    CannotParse(ParseIntError),
    LargeSeconds,
    LargeMinutes,
    DaysNotSupported,
    LongerThanTwoHours,
}

impl std::fmt::Display for MalformedTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            MalformedTime::CannotParse(e) => e.fmt(f),
            MalformedTime::LargeSeconds => write!(f, "seconds too large"),
            MalformedTime::LargeMinutes => write!(f, "minutes too large"),
            MalformedTime::DaysNotSupported => write!(f, "cannot set timer for days"),
            MalformedTime::LongerThanTwoHours => write!(f, "time longer than 2 hours"),
        }
    }
}

impl std::error::Error for MalformedTime {}

pub fn time_str_to_val(time: &str) -> Result<TimeType, MalformedTime> {
    const TWO_HOURS: TimeType = 2 * 60 * 60;

    let mut parts = time.split(':');

    if time.is_empty() {
        return Ok(0);
    }

    let first_str = parts.next().unwrap(); // UNWRAP: split returns at least one item
    if let Some(second_str) = parts.next() {
        if let Some(third_str) = parts.next() {
            let hours: TimeType = first_str.parse().map_err(MalformedTime::CannotParse)?;
            let minutes: TimeType = second_str.parse().map_err(MalformedTime::CannotParse)?;
            let seconds: TimeType = third_str.parse().map_err(MalformedTime::CannotParse)?;

            if parts.next().is_some() {
                Err(MalformedTime::DaysNotSupported)
            } else if seconds >= 60 {
                Err(MalformedTime::LargeSeconds)
            } else if minutes >= 60 {
                Err(MalformedTime::LargeMinutes)
            } else {
                let time = seconds + 60 * (minutes + 60 * hours);
                if time > TWO_HOURS {
                    Err(MalformedTime::LongerThanTwoHours)
                } else {
                    Ok(time)
                }
            }
        } else {
            let minutes: TimeType = first_str.parse().map_err(MalformedTime::CannotParse)?;
            let seconds: TimeType = second_str.parse().map_err(MalformedTime::CannotParse)?;

            if seconds >= 60 {
                Err(MalformedTime::LargeSeconds)
            } else {
                let time = seconds + 60 * minutes;
                if time > TWO_HOURS {
                    Err(MalformedTime::LongerThanTwoHours)
                } else {
                    Ok(time)
                }
            }
        }
    } else {
        let time: TimeType = first_str.parse().map_err(MalformedTime::CannotParse)?;
        if time > TWO_HOURS {
            Err(MalformedTime::LongerThanTwoHours)
        } else {
            Ok(time)
        }
    }
}

pub fn time_to_str_val(time: TimeType) -> String {
    let millis = (time % 1000) / 10;
    let seconds = (time / 1000) % 60;
    let minutes = ((time / 1000) / 60) % 60;
    let hours = time / 1000 / 60 / 60;
    format!("{hours:02}:{minutes:02}:{seconds:02}.{millis:02}")
}

pub fn time_to_str_val_without_millis(time: TimeType) -> String {
    let seconds = (time / 1000) % 60;
    let minutes = ((time / 1000) / 60) % 60;
    let hours = time / 1000 / 60 / 60;
    format!("{hours:02}:{minutes:02}:{seconds:02}")
}

fn sound_cue_minute(clock_val: TimeType, previous_clock_val: TimeType) -> Option<Sound> {
    sound_cue(60 * 1000, clock_val, previous_clock_val).then_some(Sound::One)
}

fn sound_cue_minutes(clock_val: TimeType, previous_clock_val: TimeType) -> Option<Sound> {
    const MARKS: &[(TimeType, Sound)] = &[
        (2 * 60 * 1000, Sound::Two),
        (3 * 60 * 1000, Sound::Three),
        (4 * 60 * 1000, Sound::Four),
        (5 * 60 * 1000, Sound::Five),
        (10 * 60 * 1000, Sound::Ten),
        (30 * 60 * 1000, Sound::Thirty),
    ];
    for (mark, sound) in MARKS.iter() {
        let cue = sound_cue(*mark, clock_val, previous_clock_val).then_some(*sound);
        if cue.is_some() {
            return cue;
        }
    }
    None
}

fn sound_cue_seconds(clock_val: TimeType, previous_clock_val: TimeType) -> Option<Sound> {
    const MARKS: &[(TimeType, Sound)] = &[
        (1000, Sound::One),
        (2 * 1000, Sound::Two),
        (3 * 1000, Sound::Three),
        (4 * 1000, Sound::Four),
        (5 * 1000, Sound::Five),
        (10 * 1000, Sound::Ten),
        (30 * 1000, Sound::Thirty),
    ];
    for (mark, sound) in MARKS.iter() {
        let cue = sound_cue(*mark, clock_val, previous_clock_val).then_some(*sound);
        if cue.is_some() {
            return cue;
        }
    }
    None
}

fn sound_cue(millis: TimeType, clock_val: TimeType, previous_clock_val: TimeType) -> bool {
    const MARGIN: TimeType = 0;
    previous_clock_val > (millis + MARGIN) && clock_val <= (millis + MARGIN)
}
