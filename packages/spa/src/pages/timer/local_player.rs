use crate::{pages::timer::hooks::TOO_LONG, prelude::*};

#[derive(Debug, Clone, PartialEq, Properties)]
pub struct Props {
    pub name: AttrValue,
    pub score: AttrValue,

    pub set_name: Callback<String>,
    pub set_score: Callback<String>,
}

#[function_component]
pub fn Component(props: &Props) -> Html {
    let Props {
        ref name,
        ref score,
        ref set_name,
        ref set_score,
    } = props;

    let name_validation = use_state_eq(String::new);
    let score_validation = use_state_eq(String::new);

    let check_name = Callback::from({
        let name_validation = name_validation.clone();
        move |new_name: String| {
            if new_name.len() >= TOO_LONG {
                name_validation.set("name too long".to_string())
            } else {
                name_validation.set(String::new())
            }
        }
    });

    let check_score = Callback::from({
        let score_validation = score_validation.clone();
        move |new_score: String| {
            if new_score.len() >= TOO_LONG {
                score_validation.set("score too long".to_string())
            } else {
                score_validation.set(String::new())
            }
        }
    });

    html! {
        <div>
            <div class="field">
                <label class="label">{"Name"}</label>
                <div class="control">
                    <submit_line::Component
                        data_test="name"
                        cta="Set"
                        validation_classes="is-danger"
                        validation_message={name_validation.to_string()}
                        initial_value={name}
                        oninput={check_name}
                        onsubmit={set_name} />
                </div>
            </div>
            <div class="field">
            <label class="label">{"Score"}</label>
                <div class="control">
                    <submit_line::Component
                        data_test="score"
                        cta="Set"
                        validation_classes="is-danger"
                        validation_message={score_validation.to_string()}
                        initial_value={score}
                        oninput={check_score}
                        onsubmit={set_score} />
                </div>
            </div>
        </div>
    }
}
