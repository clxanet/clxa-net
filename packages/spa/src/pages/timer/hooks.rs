use gloo::net::websocket::{futures::WebSocket, Message};
use serde::{Deserialize, Serialize};
use std::{cell::RefCell, collections::BTreeMap, rc::Rc};
use yew::platform::pinned::mpsc::{self, UnboundedSender};

use crate::{pages::timer::clock::time_to_str_val, prelude::*};
use futures::{SinkExt, StreamExt};

pub type TimeType = u64;
pub type PlayerKey = u64;

pub const PROTOCOL_VERSION: u32 = 1;

pub const TOO_LONG: usize = 31;

pub fn api_route(hostname: &str) -> String {
    if hostname.is_empty() {
        return String::new();
    }

    let secured = web_sys::window()
        .and_then(|window| window.location().protocol().ok())
        .as_deref()
        == Some("https");
    let protocol = if secured { "wss" } else { "ws" };
    format!("{protocol}://{hostname}/.multiplayer-timer/join-room")
}

#[derive(Debug, Clone, Copy, PartialEq, Serialize, Deserialize)]
pub struct TimeState {
    pub time: TimeType,
    pub is_running: bool,
    pub no_send: bool,
}

impl Default for TimeState {
    fn default() -> Self {
        Self {
            time: 300000,
            is_running: false,
            no_send: false,
        }
    }
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
pub struct ClientPlayer {
    pub name: String,
    pub score: String,
}

impl ClientPlayer {
    pub fn is_allowed(&self) -> bool {
        self.name.len() < TOO_LONG && self.score.len() < TOO_LONG
    }
}

impl Default for ClientPlayer {
    fn default() -> Self {
        Self {
            name: "Player".to_string(),
            score: "0".to_string(),
        }
    }
}

#[derive(Clone)]
pub enum SessionMessage {
    PlayerChange { name: String, content: String },
    Message { name: String, content: String },
    Countdown { name: String, content: String },
}

#[derive(Clone, Serialize, Deserialize)]
pub enum ToServer {
    Hello { room_id: String, protocol: u32 },
    Leave,
    SetPlayer(ClientPlayer),
    SetTimer(TimeState),
    Message(String),
    Ping,
}

#[derive(Clone, Serialize, Deserialize)]
pub enum FromServer {
    Hello {
        protocol: u32,
        expiry: TimeType,
    },
    SetPlayer {
        key: PlayerKey,
        client: Option<ClientPlayer>,
    },
    SetTimer {
        key: PlayerKey,
        timer: TimeState,
    },
    Message {
        key: PlayerKey,
        message: String,
    },
    Leave {
        message: String,
    },
    Ping,
}

impl ToServer {
    pub fn is_finisher(&self) -> bool {
        matches!(self, Self::Leave)
    }

    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }

    pub fn from_json(value: &str) -> Option<Self> {
        serde_json::from_str(value).ok()
    }
}

impl FromServer {
    pub fn is_finisher(&self) -> bool {
        matches!(self, Self::Leave { .. })
    }

    pub fn to_json(&self) -> String {
        serde_json::to_string(self).unwrap()
    }

    pub fn from_json(value: &str) -> Option<Self> {
        serde_json::from_str(value).ok()
    }
}

struct Connection {
    sender: UnboundedSender<ToServer>,
    client: ClientPlayer,
    set_timer: UseStateSetter<TimeState>,
    timer: Rc<RefCell<TimeState>>,
    url: String,
}

struct ConnectionRefs {
    set_timer: UseStateSetter<TimeState>,
    timer: Rc<RefCell<TimeState>>,
    players: Rc<RefCell<BTreeMap<PlayerKey, ClientPlayer>>>,
    messages: Rc<RefCell<Vec<SessionMessage>>>,
    error_message: UseStateHandle<String>,
    room_expiry: Rc<RefCell<(f64, TimeType)>>,
}

impl Connection {
    pub fn new(
        url: String,
        room_id: String,
        client: ClientPlayer,

        refs: ConnectionRefs,
    ) -> Option<Self> {
        let ws = WebSocket::open(&url).ok()?;
        let (mut write, mut read) = ws.split();
        let (sender, mut rx) = mpsc::unbounded::<ToServer>();

        let ConnectionRefs {
            set_timer,
            timer,
            players,
            messages,
            error_message,
            room_expiry,
        } = refs;

        yew::platform::spawn_local({
            let client = client.clone();
            async move {
                gloo::console::log!("starting ws sender task");

                if let Err(e) = write
                    .send(Message::Text(
                        ToServer::Hello {
                            room_id,
                            protocol: PROTOCOL_VERSION,
                        }
                        .to_json(),
                    ))
                    .await
                {
                    gloo::console::error!("failed hello", e.to_string());
                    return;
                }

                if let Err(e) = write
                    .send(Message::Text(ToServer::SetPlayer(client).to_json()))
                    .await
                {
                    gloo::console::error!("failed initial SetPlayer", e.to_string());
                    return;
                }

                while let Some(msg) = rx.next().await {
                    if let Err(e) = write.send(Message::Text(msg.to_json())).await {
                        gloo::console::error!(e.to_string());
                        break;
                    }

                    if msg.is_finisher() {
                        break;
                    }
                }
                gloo::console::log!("finished ws sender task");
            }
        });

        yew::platform::spawn_local({
            let set_timer = set_timer.clone();
            let timer = timer.clone();
            let sender = sender.clone();
            async move {
                gloo::console::log!("starting ws receiver task");
                while let Some(msg) = read.next().await {
                    match msg {
                        Ok(Message::Text(value)) => {
                            if let Some(msg) = FromServer::from_json(&value) {
                                match msg {
                                    FromServer::Hello {
                                        protocol: PROTOCOL_VERSION,
                                        expiry,
                                    } => {
                                        gloo::console::log!("new hello", expiry);
                                        players.borrow_mut().clear();
                                        messages.borrow_mut().clear();
                                        *room_expiry.borrow_mut() = (js_sys::Date::now(), expiry);
                                    }
                                    FromServer::Hello { .. } => {
                                        gloo::console::error!("unknown protocol");
                                        return;
                                    }
                                    FromServer::SetPlayer { key, client } => {
                                        if let Some(client) = client {
                                            let previous =
                                                players.borrow_mut().insert(key, client.clone());
                                            match (previous, client) {
                                                (None, ClientPlayer { name, .. }) => {
                                                    messages.borrow_mut().insert(
                                                        0,
                                                        SessionMessage::PlayerChange {
                                                            name,
                                                            content: "joined".to_string(),
                                                        },
                                                    );
                                                }
                                                (
                                                    Some(ClientPlayer {
                                                        name: old_name,
                                                        score: old_score,
                                                    }),
                                                    ClientPlayer { name, score },
                                                ) => {
                                                    if old_name != name {
                                                        messages.borrow_mut().insert(
                                                            0,
                                                            SessionMessage::PlayerChange {
                                                                name: old_name,
                                                                content: format!(
                                                                    "changed name to {}",
                                                                    name
                                                                ),
                                                            },
                                                        );
                                                    }
                                                    if old_score != score {
                                                        messages.borrow_mut().insert(
                                                            0,
                                                            SessionMessage::PlayerChange {
                                                                name,
                                                                content: format!(
                                                                    "changed score from {} to {}",
                                                                    old_score, score
                                                                ),
                                                            },
                                                        );
                                                    }
                                                }
                                            }
                                        } else if let Some(ClientPlayer { name, .. }) =
                                            players.borrow_mut().remove(&key)
                                        {
                                            messages.borrow_mut().insert(
                                                0,
                                                SessionMessage::PlayerChange {
                                                    name,
                                                    content: "left".to_string(),
                                                },
                                            );
                                        }

                                        // triggers redraw and make spa show room controls
                                        error_message.set(String::new());
                                    }
                                    FromServer::SetTimer {
                                        key,
                                        timer: new_timer,
                                    } => {
                                        if let Some(player) = players.borrow().get(&key) {
                                            if timer.borrow().time != new_timer.time {
                                                messages.borrow_mut().insert(
                                                    0,
                                                    SessionMessage::Countdown {
                                                        name: player.name.clone(),
                                                        content: format!(
                                                            "stopped timer on {}",
                                                            time_to_str_val(new_timer.time),
                                                        ),
                                                    },
                                                );
                                            }
                                            if timer.borrow().is_running != new_timer.is_running
                                                && new_timer.is_running
                                            {
                                                messages.borrow_mut().insert(
                                                    0,
                                                    SessionMessage::Countdown {
                                                        name: player.name.clone(),
                                                        content: "started the timer".to_string(),
                                                    },
                                                );
                                            }
                                        }
                                        set_timer.set(new_timer);
                                        *timer.borrow_mut() = new_timer;
                                    }
                                    FromServer::Message { key, message } => {
                                        if let Some(player) = players.borrow().get(&key) {
                                            messages.borrow_mut().insert(
                                                0,
                                                SessionMessage::Message {
                                                    name: player.name.clone(),
                                                    content: message,
                                                },
                                            );
                                            // trigger redraw
                                            error_message.set(String::new());
                                        }
                                    }
                                    FromServer::Leave { message } => {
                                        error_message.set(message);
                                        break;
                                    }
                                    FromServer::Ping => {
                                        sender.send_now(ToServer::Ping).ok();
                                    }
                                }
                            } else {
                                gloo::console::error!("cannot parse message:", value);
                            }
                        }
                        Err(gloo::net::websocket::WebSocketError::ConnectionClose(_)) => {
                            error_message.set("No connection.".to_string());
                            break;
                        }
                        x => {
                            gloo::console::log!(format!("unexpected message: {x:?}"));
                        }
                    }
                }
                gloo::console::log!("finished ws receiver task");
            }
        });

        Some(Self {
            sender,
            client,
            set_timer,
            timer,
            url,
        })
    }

    pub fn set_client(&mut self, client: ClientPlayer) {
        if self.client != client {
            gloo::console::log!("sending new client info");
            self.sender
                .send_now(ToServer::SetPlayer(client.clone()))
                .ok();
            self.client = client;
        }
    }

    pub fn set_timer(&mut self, timer: UseStateHandle<TimeState>) {
        if *self.timer.borrow() != *timer && !timer.no_send {
            self.sender.send_now(ToServer::SetTimer(*timer)).ok();
            self.set_timer = timer.setter();
        }
    }

    pub fn is_connection_to(&self, url: &str) -> bool {
        self.url == url
    }
}

impl Drop for Connection {
    fn drop(&mut self) {
        self.sender.send_now(ToServer::Leave).ok();
    }
}

#[hook]
pub fn use_connection(hostname: &str, room_id: &str) -> UseConnectionHandle {
    let socket = use_mut_ref(|| Option::<Connection>::None);
    let error_message = use_state(|| "connecting to room...".to_string());
    let room_id = room_id.to_string();

    let name = use_state(|| ClientPlayer::default().name);
    let score = use_state(|| ClientPlayer::default().score);
    let timer = use_state(TimeState::default);
    let set_timer = timer.setter();
    let timer_mut = use_mut_ref(TimeState::default);
    let players = use_mut_ref(BTreeMap::<PlayerKey, ClientPlayer>::default);
    let messages = use_mut_ref(Vec::<SessionMessage>::default);
    let room_expiry = use_mut_ref(<(f64, TimeType)>::default);

    use_effect({
        let hostname = hostname.to_string();
        let error_message = error_message.clone();
        let client = ClientPlayer {
            name: name.to_string(),
            score: score.to_string(),
        };
        let timer = timer.clone();
        let socket = socket.clone();

        let players = players.clone();
        let messages = messages.clone();
        let room_expiry = room_expiry.clone();
        move || {
            let url = api_route(&hostname);
            let mut socket = socket.borrow_mut();
            let client = client.clone();
            let error_message = error_message.clone();
            if let Some(con) = &mut *socket {
                if con.is_connection_to(&url) {
                    con.set_client(client);
                    con.set_timer(timer);
                } else {
                    *socket = Connection::new(
                        url,
                        room_id,
                        client,
                        ConnectionRefs {
                            timer: timer_mut,
                            set_timer,
                            error_message,
                            players,
                            messages,
                            room_expiry,
                        },
                    );
                }
            } else {
                *socket = Connection::new(
                    url,
                    room_id,
                    client,
                    ConnectionRefs {
                        timer: timer_mut,
                        set_timer,
                        error_message,
                        players,
                        messages,
                        room_expiry,
                    },
                );
            }
            || {}
        }
    });

    UseConnectionHandle {
        socket,
        name,
        score,
        timer,
        error_message,
        players,
        messages,
        room_expiry,
    }
}

pub struct UseConnectionHandle {
    socket: Rc<RefCell<Option<Connection>>>,
    name: UseStateHandle<String>,
    score: UseStateHandle<String>,
    timer: UseStateHandle<TimeState>,
    error_message: UseStateHandle<String>,
    players: Rc<RefCell<BTreeMap<PlayerKey, ClientPlayer>>>,
    messages: Rc<RefCell<Vec<SessionMessage>>>,
    room_expiry: Rc<RefCell<(f64, TimeType)>>,
}

impl UseConnectionHandle {
    pub fn name_state(&self) -> (String, Callback<String>) {
        ((*self.name).clone(), {
            let name = self.name.clone();
            Callback::from(move |new_name| name.set(new_name))
        })
    }

    pub fn score_state(&self) -> (String, Callback<String>) {
        ((*self.score).clone(), {
            let score = self.score.clone();
            Callback::from(move |new_score| score.set(new_score))
        })
    }

    pub fn error_message(&self) -> String {
        self.error_message.to_string()
    }

    pub fn time_state(&self) -> (TimeState, Callback<TimeState>) {
        (*self.timer, {
            let timer = self.timer.clone();
            Callback::from(move |new_time: TimeState| {
                timer.set(new_time);
            })
        })
    }

    pub fn log_state(&self) -> (Rc<RefCell<Vec<SessionMessage>>>, Callback<String>) {
        let socket = self.socket.clone();
        (
            self.messages.clone(),
            Callback::from(move |msg: String| {
                if let Some(connection) = &mut *socket.borrow_mut() {
                    connection.sender.send_now(ToServer::Message(msg)).ok();
                }
            }),
        )
    }

    pub fn all_players(&self) -> Vec<ClientPlayer> {
        self.players.borrow().values().cloned().collect()
    }

    pub fn room_expiry(&self) -> (f64, TimeType) {
        *self.room_expiry.borrow()
    }
}
