use std::time::Duration;

use axum::extract::ws::{Message, WebSocket};
use futures::{stream::FuturesUnordered, SinkExt, StreamExt};
use spa::pages::timer::hooks::{FromServer, PlayerKey, TimeState, ToServer};
use tokio::{sync::mpsc, time::Instant};

use crate::room::Room;

pub const EXPIRES_MARGIN: Duration = Duration::from_secs(15);
pub const EMPTY_ROOM_DURATION: Duration = Duration::from_secs(60);
pub const MAX_ROOM_DURATION: Duration = Duration::from_secs(2 * 60 * 60);
pub const MAX_SEND_DURATION: Duration = Duration::from_secs(15);

pub async fn player_task(
    this: Room,
    key: PlayerKey,
    socket: WebSocket,
    _send: mpsc::Sender<FromServer>,
    mut on_send: mpsc::Receiver<FromServer>,
    publish: mpsc::Sender<FromServer>,
    introductions: Option<Vec<FromServer>>,
) -> Option<()> {
    let (mut tx, mut rx) = socket.split();
    if let Some(introductions) = introductions {
        for msg in introductions.into_iter() {
            tx.send(Message::Text(msg.to_json())).await.ok()?;
        }
    } else {
        tokio::spawn(async move {
            tx.send(Message::Text(
                FromServer::Leave {
                    message: "room is full".to_string(),
                }
                .to_json(),
            ))
            .await
            .ok();
        });
        return None;
    }

    let tx_task = async move {
        while let Some(msg) = on_send.recv().await {
            tx.send(Message::Text(msg.to_json())).await.ok()?;
        }
        Some(())
    };

    let rx_task = async move {
        while let Some(ws_msg) = rx.next().await {
            match ws_msg {
                Ok(Message::Text(text)) => match ToServer::from_json(&text)? {
                    ToServer::Hello { .. } => return None,
                    ToServer::Leave => return Some(()),
                    ToServer::SetPlayer(client) => {
                        if client.is_allowed() {
                            publish
                                .send(FromServer::SetPlayer {
                                    key,
                                    client: Some(client),
                                })
                                .await
                                .ok()?
                        }
                    }

                    ToServer::SetTimer(timer) => publish
                        .send(FromServer::SetTimer {
                            key,
                            timer: TimeState {
                                no_send: true,
                                ..timer
                            },
                        })
                        .await
                        .ok()?,
                    ToServer::Message(message) => {
                        if !message.is_empty() {
                            publish
                                .send(FromServer::Message { key, message })
                                .await
                                .ok()?
                        }
                    }
                    ToServer::Ping => {}
                },
                x @ Ok(_) | x @ Err(_) => {
                    tracing::info!(?x);
                    return None;
                }
            }
        }
        Some(())
    };

    let res = tokio::select! {
        res = rx_task => {
            tracing::info!("rx_task finished");
            res}
        res = tx_task => {
            tracing::info!("tx_task finished");
            res}
    };
    this.remove_player(key).await;
    res
}

pub async fn room_task(this: Room) {
    loop {
        let mut on_publish = this.0.lock().await.on_publish.take().unwrap();
        let room_data_sender = wait_for_last_send(this.clone(), &mut on_publish);
        let players_changed = wait_for_players_change(this.clone());
        let room_close_time = wait_for_room_close_time(this.clone());
        let room_pings = wait_for_room_ping(this.clone());

        tokio::select! {
            biased;
            // need to replace sub tasks to new player list
            _ = players_changed => {
                this.0.lock().await.on_publish = Some(on_publish);
                continue;
            }
            _ = room_close_time => {}
            _ = room_data_sender => {}
            _ = room_pings => {}
        }

        let (key, parent) = {
            let state = this.0.lock().await;
            (state.key, state.parent.clone())
        };

        // remove this room
        let mut parent = parent.0.lock().await;
        parent.rooms.remove(&key);

        // and stop its task
        break;
    }
}

async fn wait_for_room_close_time(this: Room) {
    let (created_at, left_at, is_empty) = {
        let state = this.0.lock().await;
        (state.created_at, state.left_at, state.players.is_empty())
    };
    if is_empty {
        tokio::time::sleep_until(left_at + EMPTY_ROOM_DURATION).await
    } else {
        tokio::time::sleep_until(created_at + MAX_ROOM_DURATION + EXPIRES_MARGIN).await
    }
}

async fn wait_for_room_ping(this: Room) {
    loop {
        tokio::time::sleep(Duration::from_secs(10)).await;
        let state = this.0.lock().await;
        if state.publish.send(FromServer::Ping).await.is_err() {
            break;
        }
    }
}

async fn wait_for_players_change(this: Room) {
    let notify = { this.0.lock().await.players_changed.clone() };
    notify.notified().await;
    let _sync = this.0.lock().await;
}

async fn wait_for_last_send(this: Room, on_publish: &mut mpsc::Receiver<FromServer>) {
    let mut sends: Vec<_> = {
        let state = this.0.lock().await;
        state
            .players
            .values()
            .map(|p| (p.key, p.send.clone()))
            .collect()
    };

    while let Some(msg) = on_publish.recv().await {
        let mut state = this.0.lock().await;
        match &msg {
            FromServer::SetPlayer {
                key,
                client: Some(client),
            } => {
                if let Some(player) = state.players.get_mut(key) {
                    player.client = client.clone();
                }
            }
            FromServer::SetTimer { timer, .. } => {
                state.time = *timer;
                state.time_at = Instant::now();
            }
            _ => {}
        }

        let this = this.clone();
        let msg = msg.clone();
        let mut futs: FuturesUnordered<_> = sends
            .iter_mut()
            .map(|(key, send)| {
                let this = this.clone();
                let msg = msg.clone();
                let key = *key;
                let send = send.clone();
                async move {
                    if !matches!(
                        tokio::time::timeout(MAX_SEND_DURATION, send.send(msg.clone())).await,
                        Ok(Ok(())),
                    ) {
                        this.clone().remove_player(key).await;
                    }
                }
            })
            .collect();
        tokio::spawn(async move { while futs.next().await.is_some() {} });

        // release state here to block new joining from dropping this task mid operation
        // potentially missing a message
    }
}
