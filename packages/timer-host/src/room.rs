use std::{collections::HashMap, sync::Arc};

use axum::extract::ws::WebSocket;
use rand::{thread_rng, Rng};
use spa::pages::timer::hooks::{
    ClientPlayer, FromServer, PlayerKey, TimeState, TimeType, PROTOCOL_VERSION,
};
use timer_spa::{RoomSummary, ServerSummary};
use tokio::{
    sync::{mpsc, Mutex, Notify},
    time::Instant,
};

use crate::{old_tasks::MAX_ROOM_DURATION, player::ServerPlayer};

#[derive(Clone, Default)]
#[allow(dead_code)]
pub struct AllRooms(pub Arc<Mutex<State>>);

pub struct State {
    pub max_room_count: usize,
    pub new_room_max_player_count: usize,
    pub rooms: HashMap<RoomId, Room>,
}

impl Default for State {
    fn default() -> Self {
        Self {
            max_room_count: 500,
            new_room_max_player_count: 20,
            rooms: Default::default(),
        }
    }
}

impl AllRooms {
    pub async fn get_summary(&self) -> ServerSummary {
        let state = self.0.lock().await;
        let max_room_count = state.max_room_count;

        let mut rooms = vec![];
        for (room_id, room) in state.rooms.iter() {
            let inner = room.0.lock().await;
            let room_id = *room_id;
            let current_player_count = inner.players.len();
            let max_player_count = inner.max_player_count;
            let expires_in = MAX_ROOM_DURATION
                .saturating_sub(Instant::now().saturating_duration_since(inner.created_at))
                .as_millis() as TimeType;

            rooms.push(RoomSummary {
                present: true,
                room_id,
                current_player_count,
                max_player_count,
                expires_in,
            });
        }

        ServerSummary {
            max_room_count,
            rooms,
        }
    }

    pub async fn create_new_room(&self, force_create: bool) -> Option<String> {
        Some(
            Room::create_in(self.clone(), force_create)
                .await?
                .to_string(),
        )
    }

    pub async fn join_room(&self, room_id: String, socket: WebSocket) -> Option<()> {
        let room = {
            let state = self.0.lock().await;
            state.rooms.get(&room_id.parse().ok()?)?.clone()
        };

        room.add_player(socket).await;
        Some(())
    }
}

pub type RoomId = u32;

#[derive(Clone)]
pub struct Room(pub Arc<Mutex<Inner>>);

pub struct Inner {
    pub key: RoomId,
    pub max_player_count: usize,
    pub parent: AllRooms,
    pub players: HashMap<PlayerKey, ServerPlayer>,
    pub players_changed: Arc<Notify>,
    pub time: TimeState,
    pub time_at: Instant,
    pub created_at: Instant,
    pub left_at: Instant,
    pub publish: mpsc::Sender<FromServer>,
    pub on_publish: Option<mpsc::Receiver<FromServer>>,
}

impl Room {
    async fn create_in(rooms: AllRooms, force_create: bool) -> Option<RoomId> {
        let now = Instant::now();
        let parent = rooms.clone();
        let mut state = rooms.0.lock().await;

        if !force_create && state.rooms.len() >= state.max_room_count {
            return None;
        }

        let mut key = thread_rng().gen();
        while state.rooms.contains_key(&key) {
            key = thread_rng().gen()
        }

        let (publish, on_publish) = mpsc::channel(1);

        let inner = Inner {
            key,
            max_player_count: state.new_room_max_player_count,
            parent,
            created_at: now,
            left_at: now,
            time: TimeState::default(),
            time_at: now,
            players: Default::default(),
            players_changed: Default::default(),
            publish,
            on_publish: Some(on_publish),
        };

        let room = Room(Arc::new(Mutex::new(inner)));
        state.rooms.insert(key, room.clone());
        tokio::spawn(crate::old_tasks::room_task(room));
        Some(key)
    }

    pub async fn remove_player(&self, key: PlayerKey) {
        let mut inner = self.0.lock().await;

        if let Some(_player) = inner.players.get(&key) {
            // player.par_send_removal(reason);
        }
        // inner.par_send_removal();
        inner.players.remove(&key);
        inner.left_at = Instant::now();
        inner.players_changed.notify_waiters();
    }

    pub async fn add_player(self, socket: WebSocket) {
        let mut inner = self.0.lock().await;

        if inner.players.len() >= inner.max_player_count {
            return;
        }

        let mut key = thread_rng().gen();
        while key == 0 || inner.players.contains_key(&key) {
            key = thread_rng().gen();
        }

        let elapsed_time = inner.elapsed_time();
        let introductions = inner.create_introductions_vec(elapsed_time);
        let publish = inner.publish.clone();
        let (send, on_send) = mpsc::channel(1);

        let player = ServerPlayer {
            key,
            send: send.clone(),
            client: ClientPlayer::default(),
        };

        inner.players.insert(key, player);
        inner.players_changed.notify_waiters();
        std::mem::drop(inner);

        tokio::spawn(crate::old_tasks::player_task(
            self,
            key,
            socket,
            send,
            on_send,
            publish,
            Some(introductions),
        ));
    }
}

impl Inner {
    fn create_introductions_vec(&self, elapsed_time: TimeState) -> Vec<FromServer> {
        let mut v = vec![
            FromServer::Hello {
                protocol: PROTOCOL_VERSION,
                expiry: MAX_ROOM_DURATION
                    .saturating_sub(Instant::now().saturating_duration_since(self.created_at))
                    .as_millis() as TimeType,
            },
            FromServer::SetTimer {
                key: 0,
                timer: elapsed_time,
            },
        ];
        v.extend(self.players.iter().map(|(key, player)| {
            tracing::info!(?key, client = ?player.client, "introducing");
            FromServer::SetPlayer {
                key: *key,
                client: Some(player.client.clone()),
            }
        }));
        v
    }

    fn elapsed_time(&self) -> TimeState {
        if self.time.is_running {
            TimeState {
                is_running: true,
                time: self.time.time.saturating_sub(
                    Instant::now()
                        .saturating_duration_since(self.time_at)
                        .as_millis() as TimeType,
                ),
                no_send: true,
            }
        } else {
            self.time
        }
    }
}
