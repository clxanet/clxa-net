mod api;
mod old_tasks;
mod player;
mod room;
mod serve;

use std::net::SocketAddr;

use axum::Server;
use clap::Parser;
use tracing_subscriber::prelude::*;

/// Backend serving clxa.net timer services and pages
#[derive(Parser)]
struct Cli {
    /// address and port to bind to
    #[clap(short, long, default_value = "127.0.0.1:8100")]
    pub bind: SocketAddr,

    /// internally used hostname during server side rendering
    #[clap(short, long, default_value = "localhost:8100")]
    pub authority: String,
}

#[tokio::main(flavor = "current_thread")]
async fn main() {
    let cli = Cli::parse();

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::try_from_default_env().unwrap_or_else(|_| "info".into()),
        )
        .with(tracing_subscriber::fmt::layer())
        .init();

    let all_rooms = room::AllRooms::default();
    let app = api::routes(all_rooms.clone());
    let app = app.fallback_service(serve::routes(cli.authority, all_rooms.clone()));

    tracing::info!("listening for timer API calls on {}", cli.bind);

    Server::bind(&cli.bind)
        .serve(app.into_make_service())
        .with_graceful_shutdown(async {
            tokio::signal::ctrl_c().await.unwrap();
        })
        .await
        .unwrap();
}
