use std::{borrow::Cow, collections::HashMap, sync::Arc};

use axum::{
    extract::{Path, Query},
    http::{header, HeaderValue, StatusCode},
    response::IntoResponse,
    routing::get,
    Router,
};
use include_dir::{include_dir, Dir};

use crate::room::AllRooms;

static DIST_DIR: Dir = include_dir!("$CARGO_MANIFEST_DIR/../timer-spa/dist/");

pub fn routes(authority: String, rooms: AllRooms) -> Router {
    let mut router = Router::new();
    let mut not_found = None;
    for file in DIST_DIR.files() {
        let mut route = file.path().to_string_lossy();
        if route == "index.html" {
            route = "".into();
            let content = file.contents_utf8().unwrap().to_string();
            let rooms = rooms.clone();
            not_found = Some(ServeFile {
                file_path: "",
                content: Arc::new(MaybeString::Yes(content)),
                status_code: StatusCode::NOT_FOUND,
                rooms,
            });
        }
        let file_path = route.to_string();
        let content = file.contents().to_vec();
        let authority = authority.clone();
        let rooms = rooms.clone();
        router = router.route(
            &format!("/{route}"),
            get(
                move |Query(args): Query<HashMap<String, String>>| async move {
                    ServeFile {
                        file_path: &file_path,
                        content: Arc::new(MaybeString::No(content)),
                        status_code: StatusCode::OK,
                        rooms,
                    }
                    .serve(authority.clone(), file_path.to_string(), args)
                    .await
                },
            ),
        )
    }
    router.route(
        "/*rest",
        get(
            move |Path(url): Path<String>, Query(args): Query<HashMap<String, String>>| async move {
                not_found
                    .clone()
                    .unwrap()
                    .serve(authority.to_string(), url.to_string(), args)
                    .await
            },
        ),
    )
}

enum MaybeString {
    Yes(String),
    No(Vec<u8>),
}

impl MaybeString {
    pub fn to_vec(&self) -> Vec<u8> {
        match self {
            Self::Yes(s) => s.as_bytes().to_vec(),
            Self::No(v) => v.to_vec(),
        }
    }

    pub fn as_str(&self) -> Cow<'_, str> {
        match self {
            Self::Yes(s) => s.into(),
            Self::No(v) => String::from_utf8_lossy(v),
        }
    }
}

#[derive(Clone)]
struct ServeFile<'a> {
    file_path: &'a str,
    content: Arc<MaybeString>,
    status_code: StatusCode,
    rooms: AllRooms,
}

impl<'a> ServeFile<'a> {
    async fn serve(
        self,
        authority: String,
        url: String,
        args: HashMap<String, String>,
    ) -> impl IntoResponse {
        let (content_type, content) = if self.file_path.is_empty() {
            (
                "text/html",
                Self::ssr(self.content, authority, url, args, self.rooms.clone()).await,
            )
        } else {
            let suffix = self.file_path.rsplit('.').next();
            let content_type = match suffix {
                Some("css") => "text/css",
                Some("ogg") => "audio/ogg",
                Some("js") => "application/javascript",
                Some("wasm") => "application/wasm",
                Some("png") => "image/png",
                _ => "text/plain",
            };
            (content_type, self.content.to_vec())
        };

        (
            self.status_code,
            [(header::CONTENT_TYPE, HeaderValue::from_static(content_type))],
            content,
        )
    }

    async fn ssr(
        template: Arc<MaybeString>,
        host: String,
        url: String,
        args: HashMap<String, String>,
        all_rooms: AllRooms,
    ) -> Vec<u8> {
        let props = timer_spa::Props {
            host: Some(host),
            url: Some(url),
            args: Some(args),
            rooms: Some(all_rooms.get_summary().await),
        };
        let renderer = timer_spa::yew::ServerRenderer::<timer_spa::App>::with_props(move || props);
        let template = template.as_str();
        let parts: Vec<_> = template
            .splitn(2, "<!-- server side render split -->")
            .collect();

        vec![
            parts[0].to_string(),
            renderer.render().await,
            parts[1].to_string(),
        ]
        .join("")
        .as_bytes()
        .to_vec()
    }
}
