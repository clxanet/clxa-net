use spa::pages::timer::hooks::{ClientPlayer, FromServer, PlayerKey};
use tokio::sync::mpsc;

pub struct ServerPlayer {
    pub key: PlayerKey,
    pub send: mpsc::Sender<FromServer>,
    pub client: ClientPlayer,
}
