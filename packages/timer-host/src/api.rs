use std::time::Duration;

use axum::{
    extract::{ws::Message, State, WebSocketUpgrade},
    http::StatusCode,
    response::{IntoResponse, Response},
    routing::{get, post},
    Json, Router,
};
use spa::pages::timer::hooks::{ToServer, PROTOCOL_VERSION};

use crate::room::AllRooms;

pub const MAX_SEND_DURATION: Duration = Duration::from_secs(15);

pub fn routes(all_rooms: AllRooms) -> Router {
    Router::new()
        .route("/.manage", post(manage_post))
        .route("/.multiplayer-timer/join-room", get(join_room))
        .route("/.multiplayer-timer/create-room/", post(create_room))
        .with_state(all_rooms)
}

async fn join_room(State(rooms): State<AllRooms>, ws: WebSocketUpgrade) -> Response {
    ws.on_upgrade(move |mut socket| async move {
        if let Ok(Some(Ok(Message::Text(msg)))) =
            tokio::time::timeout(MAX_SEND_DURATION, socket.recv()).await
        {
            if let Some(ToServer::Hello {
                room_id,
                protocol: PROTOCOL_VERSION,
            }) = ToServer::from_json(&msg)
            {
                rooms.join_room(room_id.clone(), socket).await;
            }
        }
    })
}

async fn create_room(State(rooms): State<AllRooms>) -> Response {
    if let Some(room_id) = rooms.create_new_room(false).await {
        spa::pages::timer::create_room::CreateRoomResponse::Created { room_id }
            .to_json()
            .into_response()
    } else {
        (
            StatusCode::SERVICE_UNAVAILABLE,
            "the server does not have available rooms",
        )
            .into_response()
    }
}

async fn manage_post(
    State(all_rooms): State<AllRooms>,
    Json(config): Json<timer_spa::SetRoomConfig>,
) -> impl IntoResponse {
    match config {
        timer_spa::SetRoomConfig::MaxPlayerCount { room_id, count } => {
            let room = all_rooms.0.lock().await.rooms.get(&room_id).cloned();
            if let Some(room) = room {
                let mut inner = room.0.lock().await;
                inner.max_player_count = count;
            }
        }
        timer_spa::SetRoomConfig::MaxRoomCount { count } => {
            let mut state = all_rooms.0.lock().await;
            state.max_room_count = count;
        }
        timer_spa::SetRoomConfig::ForceCreateRoom => {
            all_rooms.create_new_room(true).await;
        }
    }
    "".into_response()
}
